<?php

if(!empty($_GET['xml'])){
	
	$archive_file_name = basename($_GET['xml'] . '.zip');
	$archive_file_path = realpath('../headers') . '\\' . $archive_file_name;
} else {
	echo 'Input file missing. Download aborted.';
	die();
}


if (file_exists($archive_file_path)) {
	//proceed
} else {
	echo 'Error while retrieing the archive file [' . $archive_file_path . ']. Action aborted.';
	die();
}


	try {

		header("Content-type: application/zip");
		header("Content-Disposition: attachment; filename=$archive_file_name");
		header("Content-length: " . filesize($archive_file_path));
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile("$archive_file_path");
// echo 'filename: ' . $archive_file_name;
// echo 'full path: ' . $archive_file_path;

	} catch (Exception $e) {
		echo $e->getMessage();
	}
	
