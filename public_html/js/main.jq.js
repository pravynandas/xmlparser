/**
 * prerequisite: jQuery
 * Called in src/main.php
 */
$(function(){
	
	var tableOffset = $("#table-1").offset().top;
	var $header = $("#table-1 > thead").clone();
	var $fixedHeader = $("#header-fixed").append($header);
 
	$(window).bind("scroll", function() {
	    var offset = $(this).scrollTop();
	    //alert(offset);
	    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
	        $fixedHeader.show();
	    }
	    else if (offset < tableOffset) {
	        $fixedHeader.hide();
	    }
	});


 	$.fn.disableSelection = function() {
//        return this
//                 .attr('unselectable', 'on')
//                 .css('user-select', 'none')
//                 .on('selectstart', false);
 		return true;
    };
    
	$( '.expandable' ).each(function() {
			if ( $(this).height() >= 600) {
				$(this).attr("title","Double click to see more..");
				$(this).addClass("dblclck_div");
			}
	});
	$( '.expandable' ).click(function(){
		$(this).disableSelection();
	});

});