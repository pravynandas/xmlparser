/**
 * Called in src/main.php
 */
	var iScrollPosX = 0;
	var iScrollPosY = 0;

	function getLastPos() {
		//alert('hello world');
		var doc = document.documentElement;
		var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
		var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
		iScrollPosX = left;
		iScrollPosY = top;
		//alert(iScrollPosX + ',' + iScrollPosY);
		return true;
	}

	function gotoLastPost() {
		//alert(iScrollPosX + ',' + iScrollPosY);
		window.scrollTo(iScrollPosX, iScrollPosY);
	}

	function enablescroll(id) {
		if (document.getElementById(id).style.overflow == "scroll") {
			document.getElementById(id).scrollTop = 0;
			document.getElementById(id).style.overflow = "hidden";
			document.getElementById(id).setAttribute("title", "Double click to see more..");
		} else {
			 if (document.getElementById(id).clientHeight >= 600) {	
				document.getElementById(id).style.overflow = "scroll";
				document.getElementById(id).setAttribute("title", "Double click to scroll lock..");
			 }
		}
	}

		
