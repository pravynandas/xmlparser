<?php
defined('GLOBAL_SCHEMA_IDENTIFIER') or define('GLOBAL_SCHEMA_IDENTIFIER', 'schema1');

include_once ('../../config.php');
include_once '../../classes/HelperClass.php';

if(!empty($_GET['xml'])){
	$xmlSource = $_GET['xml'];
} else {
	$xmlSource = DEFAULT_XML_FILE_Schema1;
}

if(function_exists('check_buffer_html')){
	$bufferexists = check_buffer_html($xmlSource);
	if ($bufferexists == true) {
		$bDeleteBuffer = false;
		if (!empty($_GET['buffer'])){
			if ($_GET['buffer'] == '2'){
				$bDeleteBuffer = true;	
			}
		}
		if($bDeleteBuffer){
			unlink(BUFFER_HTML_FILE_NAME);
			clearstatcache();
			//parent.window.location.reload();
			//proceed further
		}else{
			die();
		}
	} else {
		//create buffer
	} 
} else {
	echo 'Buffer checking function does not exists. Action aborted';
	die;
}
?>

<?php
include_once ('render_xml_to_html.php');

if(function_exists('render_xml_data')){
	//continue
	// start the buffering //
	ob_start();
}else{
	echo 'Required function is missing. Please check. Action aborted.';//allows the page to continue rendering
	die();
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
	<title>Using PHP SimpleXml to create HTML from XML data</title>
	<meta http-equiv="Content-Type" lang="en" content="text/html; charset=utf-8" />
	<meta name="language" content="en"/>
	<meta name="author" content="Praveen Kumar Nandagiri" />
	
	<link rel="stylesheet" href="/public_html/css/jquery-ui.css" />
	<script src="/public_html/js/jquery.min.js"></script>
	<script src="/public_html/js/jquery-ui.js"></script>
	<script src="http://malsup.github.io/jquery.blockUI.js"></script>

<!-- 	<script type="text/javascript" src="https://www.google.com/jsapi"></script> -->
<!-- 	<script type="text/javascript" -->
<!--            src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['sankey']}]}"> -->
	</script>
	
	
	
	<link rel="stylesheet" type="text/css" href="/public_html/css/style.css" title="default" media="screen" />
	<link rel="stylesheet" type="text/css" href="/public_html/css/prettify.css" />
	<script src="/public_html/js/google-code-prettify/run_prettify.js"></script>
</head>
	<body>
	<div>
	<span>XML map last modified on: <?php 
									$dt = new DateTime('@' . XML_MODIFIED_TIME_STAMP);
									$dt->setTimeZone(new DateTimeZone('America/Toronto'));
									echo $dt->format('d-M-Y h:i:s A');
									?></span>
	<span style="float: right;">This HTML last buffered on: <?php 
									$dt = new DateTime();
									$dt->setTimeZone(new DateTimeZone('America/Toronto'));
									echo $dt->format('d-M-Y h:i:s A');
									?></span>
	</div>									
		<div id="container">
			<div id="masterbox">
				<div id="content">
					<!--Your content in here-->
					<?php
							
							echo '<table id="header-fixed"></table>';
							echo '<table id="table-1">';
							echo '<thead><tr><td><h1>' . XML_TITLE_PLAIN . '</h1></td></tr></thead><tbody>';
							echo '</tbody></table>';
							echo '<table id="dummy-teable" style="display:none"><thead><tr><td></td></tr></thead></table>';

							
							//echo '<div id="sankey_multiple" style="height: 440px; overflow: scroll;"></div>';
							
							render_xml_data(XML_FILE_FULL_PATH);
					?>
				</div><!--end content-->
			</div><!--end masterbox-->
		</div><!--end container-->
		
		<script src="/public_html/js/main.js"></script>
		<script src="/public_html/js/main.jq.js"></script>
		
		
<script type="text/javascript">
$(function(){

	$.blockUI.defaults.css.top = '5%';
	$.blockUI.defaults.css.left = '5%'; 
	$(".ShowPageButton").click(function(){
		$.blockUI({ message: $(".ShowPageImage"),
			css: { 
                top:  ($(window).height() - 400) /2 + 'px', 
                left: ($(window).width() - 400) /2 + 'px', 
                //width: '400px' 
            } });
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
	});
	$(".ShowPageImage").click(function() {
		$.unblockUI();
		});
});
</script>
		
	</body>
</html>

<?php 
// Get the content that is in the buffer and put it in your file //
try {
	file_put_contents(BUFFER_HTML_FILE_NAME, ob_get_contents());
} catch (Exception $e) {
	echo $e->getMessage();
}
?>
