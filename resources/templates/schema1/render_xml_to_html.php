<?php
/*
	Written by Gary Hollands Sept 2010
	This work is available under the terms of the GNU General Public License, http://www.gnu.org/licenses/gpl.html
	render_xml_data is a function that can use XML data and present that data as HTML.
*/


	class CeiXML extends SimpleXMLElement{
		public function asHTML(){
			$ele=dom_import_simplexml($this);
			$dom = new DOMDocument('1.0', 'utf-8');
			$element=$dom->importNode($ele,true);
			$dom->appendChild($element);
			return $dom->saveHTML();
		}
	} 

	function render_xml_data($path_to_xml_file){
		
		$paramIndex = 1;  //used for param unique ids
		
		if (!file_exists($path_to_xml_file)){
			return;
		}else{
			$chars_to_replace = array('[\r]','[\t]');
			try {
				$xmlstring = trim(preg_replace($chars_to_replace, '', file_get_contents($path_to_xml_file, true)));
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
		$xml = new SimpleXMLElement($xmlstring);
		
		
		
		echo '<br /><hr>';
		
//RenderClass::Class __construct()		
		$Description = $xml->Description;
		echo '<span id="header_mapdescription"></span>';
		if(!$Description) {
			echo '<br /><span class="missing-tag">No Description tag found</span><br />';
			unset($Description);
		}
		
		echo '<br /><hr>';
		
//RenderClass::isDescriptionTagExist		
		$Variables = $xml->Variables;
		echo '<span id="header_mapvariables"></span>';
		if(!$Variables) {
			echo '<br /><span class="missing-tag">No Variable tag found</span><br />';
			unset($Variables);
//RenderClass::isVariablesTagExist			
		} else {
  			echo '<br /><h1>'. 'Variables' .'</h1>'."\n";
  			echo '<pre class="prettyprint lang-vb">';
  			foreach ($Variables->Variable as $record) {
				if ($record['public'] == 'no') {
					echo 'Private ';
				} else {
					echo 'Public ';
				}
				echo $record['name'] . ' As ' . $record['type'] . "\n";
			}
			echo '</pre><!--end painting_record-->'."\n";
		}
			
		
		echo '<br /><hr>';
		
		$MapEvents = $xml->MapEvents;
		echo '<span id="header_mapevents"></span>';
		if(!$MapEvents) {
			echo '<br /><span class="missing-tag">No MapEvents tag found</span><br />';
//RenderClass::isMapEventsTagExist			
			unset($MapEvents);
		} else {
			echo '<br /><span id="header_mapevents"><h1>MapEvents</h1></span>';
			foreach($MapEvents->Event as $event){
				echo '<div class="painting_record">'."\n";
				echo '<h2>' . $event['name'] . '</h2>';
				foreach($event->Action as $action) {
					echo '<p><span class="category">Action: </span>'. $action['name'].'</p>'."\n";
					foreach($event->Action->Parameter as $param) {
						$thisid = 'param_' . $paramIndex; $paramIndex = $paramIndex + 1;
						echo '<span class="category">Parameters[' . $param['position'] . ']: ' . $param['expression'] . '</span>'. '<br />' . 
						     '<div class="source-event expandable"><pre class="prettyprint lang-vb main-code" ondblclick="enablescroll(\'' . $thisid . '\')" id="' . $thisid . '">'. $param .'</pre></div>';
					}
				}
				echo '</div><!--end painting_record-->'."\n";
			}
		}
		
		
		echo '<br /><hr>';
		
		
		$MapSources = $xml->MapSources;
		echo '<span id="header_mapsources"></span>';
		if (!$MapSources) {
			echo '<br /><span class="missing-tag">No MapSources tag found</span><br />';
//RenderClass::isMapSourcesTagExist			
		} else {
			
			echo '<br /><h1>MapSources</h1>';
			foreach($MapSources->MapSource as $mapsource){
				echo '<h2>' . $mapsource['name'] . '</h2>';
				
				$mapschema = $mapsource->MapSchema;
				$schema = $mapschema->Schema;

				$aSourceSchemaInfo = array();
				if ($schema['connectorname']) { $aSourceSchemaInfo['connectorname'] = $schema['connectorname']; } 
				if ($schema['designedfor'])   { $aSourceSchemaInfo['designedfor'] = $schema['designedfor']; } 
				if ($schema['discriminatorrecord']) { $aSourceSchemaInfo['discriminatorrecord'] = $schema['discriminatorrecord']; } 
				if ($schema['discriminatorfield']) { $aSourceSchemaInfo['discriminatorfield'] = $schema['discriminatorfield']; }
				
				echo '<br /><br /><ul>';
				echo '<li><h2>Source Schema information</h2>'; echo '</li>';
				echo "<li><b>Connector Name: </b>" . $aSourceSchemaInfo['connectorname']; echo '</li>';
				echo "<li><b>Designed For: </b>" . $aSourceSchemaInfo['designedfor']; echo '</li>';
				if (array_key_exists('discriminatorrecord', $aSourceSchemaInfo)) {
					echo "<li><b>Discriminator Record: </b>" . $aSourceSchemaInfo['discriminatorrecord']; echo '</li>';
				} if (array_key_exists('discriminatorfield', $aSourceSchemaInfo)) {
					echo "<li><b>Discr. Field: </b>" . $aSourceSchemaInfo['discriminatorfield']; echo '</li>';
				}
				echo '</ul><br /><br />';
				
				$allrules = array();
				
				if ($schema->Rules) {
					foreach($schema->Rules->Rule as $rule) {
						$layoutname = (String) $rule['recordname'];
						$thisrule = array();
						$thisrule['name'] = (String) $rule['name'];
						$thisrule['lowvalue'] = (String) $rule['lowvalue'];
						$thisrule['operator'] = (String) $rule['operator'];
						$thisrule['casesensitive'] = (String) $rule['casesensitive'];
						$thisrule['recordname'] = $layoutname;
						
						array_push($allrules, $thisrule);
					}			
				}
				//echo '<pre>' . json_encode($allrules, TRUE) . '</pre>';
			
				$RecordLayoutEvents = $xml->xpath('/Map/MapSources/MapSource/RecordLayoutEvents');
				//while(list( , $node) = each($result)) {
				//	echo '/a/b/c: ',$node,"\n";
				//}
				$aRecordLayoutEvents = array();
				foreach($RecordLayoutEvents as $recordlayoutevent) {
					$aRecordLayoutEvent = array();
					$aRecordLayoutEvent['name'] = $recordlayoutevent['recordlayoutname'];
					
					$aEvents = array();
					foreach($recordlayoutevent->Event as $event) {
						$aEvent = array();
						$aEvent['name'] = (String) $event['name'];

						$aActions = array();
						foreach($event->Action as $action) {
							$aAction = array();
							$aAction['name'] = (String) $action['name'];

							$aParameters = array();
							foreach($action->Parameter as $parameter) {
								$aParameter = array();
								$aParameter['position'] = (String) $parameter['position'];
								$aParameter['name'] = (String) $parameter['name'];
								$aParameter['value'] = (String) $parameter;
								array_push($aParameters, $aParameter);
							}
							$aAction['parameters'] = $aParameters;
							array_push($aActions, $aAction);
						}
						$aEvent['actions'] = $aActions;
						array_push($aEvents, $aEvent);
					}
					
					$aRecordLayoutEvent['events'] = $aEvents;
					array_push($aRecordLayoutEvents, $aRecordLayoutEvent);
				}
				//echo '<pre>' . json_encode($aRecordLayoutEvents, TRUE) . '</pre>';

				
				$SourceEvents = $xml->xpath('/Map/MapSources/MapSource/SourceEvents');
				$aSourceEvents = array();
				foreach($SourceEvents as $SourceEvent) {
					$aSourceEvent = array();
					
					$aEvents = array();
					foreach($SourceEvent->Event as $event) {
						$aEvent = array();
						$aEvent['name'] = (String) $event['name'];

						$aActions = array();
						foreach($event->Action as $action) {
							$aAction = array();
							$aAction['name'] = (String) $action['name'];

							$aParameters = array();
							foreach($action->Parameter as $parameter) {
								$aParameter = array();
								$aParameter['position'] = (String) $parameter['position'];
								$aParameter['name'] = (String) $parameter['name'];
								$aParameter['value'] = (String) $parameter;
								array_push($aParameters, $aParameter);
							}
							$aAction['parameters'] = $aParameters;
							array_push($aActions, $aAction);
						}
						$aEvent['actions'] = $aActions;
						array_push($aEvents, $aEvent);
					}
					
					$aSourceEvent['events'] = $aEvents;
					array_push($aSourceEvents, $aSourceEvent);
				}
				//echo '<pre>' .  json_encode($aSourceEvents, TRUE) . '</pre>';

				/* 
				 * Display Source Events
				 */
				echo '<span id="header_sourceevents"></span>';
				foreach($aSourceEvents as $aSourceEvent) {
					foreach($aSourceEvent['events'] as $aEvent) {
					echo 'Source Event: <span class="event-name">' . $aEvent['name'] . '</span><br />';
						
						echo '<div class="parameter-row">';
						echo '<table><thead>';
						echo '<tr><td colspan="3">Action Name</td></tr>';
						echo '<tr><td>Position</td><td>Name</td><td>Value</td></tr>';
						echo '</thead><tbody>';
					
						foreach($aEvent['actions'] as $aAction) {
							echo '<tr class="action-row"><td colspan="3"> ' . $aAction['name'] . '</td></tr>';
// 							echo 'Action: ' . (String) $aAction['name'];
// 							echo '<h3>Parameters</h3>';
							foreach($aAction['parameters'] as $aParameter) {
// 								echo '<div class="parameter-row">';
// 								echo '<table><thead><tr>';
// 								echo '<td>Position</td><td>Name</td><td>Value</td>';
// 								echo '</tr></thead><tbody>';
								echo '<tr>';
								echo '<td>' . (String) $aParameter['position'] . '</td>';
								echo '<td>' . (String) $aParameter['name'] . '</td>';
								
								//Appending 'id' if the layout name is 'unknown' (to fix javascript errors)
// 								if (strtolower($aParameter['value']) == 'unknown') {
// 									$thisid = 'id' . (String) $aParameter['value'];
// 								} else {
// 									$thisid = (String) $aParameter['value'];
// 								}
								$thisid = 'target_' . (String) $aParameter['value'];

								
								if ($aParameter['name'] == 'record layout') {
									echo '<td><a href="#' . $thisid . '" onclick="return getLastPos()" >' . $aParameter['value'] . '</a></td>';
								} else if ($aParameter['name'] == 'expression' | $aParameter['name'] == 'count') {
									echo '<td><pre><code class="prettyprint lang-vb">' . $aParameter['value'] . '</pre></code></td>';
								} else {
									echo '<td>' . $aParameter['value'] . '</td>';
								}
								echo '</tr>';
							}
						}
						echo '</tbody></table></div>';
					}
				}
				
				echo '<br /><br />';
				
				$RecordLayouts = $xml->xpath('/Map/MapSources/MapSource/MapSchema/Schema/RecordLayouts');
				if ($RecordLayouts) {
					
					foreach ($RecordLayouts[0]->RecordLayout as $RecordLayout){
						$recordlayoutname = (String) $RecordLayout['name'];
					
						echo '<br /><br />';
						//Display Rules if any
						if ($allrules) {
							echo '<div class="rule-row">';
							echo '<h3>Rules</h3>';
							echo '<table><thead><tr><td>Rule Name</td><td>Low Value</td><td>Operator</td><td>Case Sesnsitive</td><td>RecordLayout Name</td></tr></thead><tbody>';
							foreach($allrules as $thisrule) {
								if ($thisrule['recordname'] == $recordlayoutname) { 
									echo '<tr>';
									echo '<td>' . $thisrule['name'] . '</td>';
									echo '<td>' . $thisrule['lowvalue'] . '</td>';
									echo '<td>' . $thisrule['operator'] . '</td>';
									echo '<td>' . $thisrule['casesensitive'] . '</td>';
									echo '<td>' . $thisrule['recordname'] . '</td>';
									echo '</tr>';
								}
							}
							echo '</tbody></table></div>';
						} else {
							echo '<span class="missing-tag">No Rules exist for this layout</span>';
						}
						
						
						$rows = array();
						foreach($RecordLayout->Fields->Field as $field) {
							$row = array();
							$row['layoutname'] = $recordlayoutname;
							$row['fieldname'] = (String) $field['name'];
							$row['datatype'] = (String) $field->Datatype['datatype'];
							$row['dataalias'] = (String) $field->Datatype['dataalias'];
							$row['dataalign'] = (String) $field->Datatype['dataalign'];
							$row['datalength'] = (String) $field->Datatype['datalength'];
							$row['dataidentify'] = (String) $field->Datatype['dataidentify'];
							$row['dataautoinc'] = (String) $field->Datatype['dataautoinc'];
							$row['datacurrency'] = (String) $field->Datatype['datacurrency'];
							$row['datarowversion'] = (String) $field->Datatype['datarowversion'];
							$row['datapadchar'] = (String) $field->Datatype['datapadchar'];
							try {
								if ($field->FieldOptions) {
									foreach ($field->FieldOptions->Option as $option){
// 										echo (String) $option['name'];
										if ((String) $option['name'] == 'defaultexpression') {
											$row['defaultval'] = (String) $option['value'];
// 											echo $row['defaultval'];
										}
									}
								}
							} catch (Exception $e) {
								echo 'Error while reading default value of a Source Record Layout ['.$recordlayoutname.'] field: ' . $e->getMessage();
							}
							array_push($rows, $row);
						}						
						
						echo '<h2>Source RecordLayout: ' . $recordlayoutname . '</h2>';
						echo '<div class="source-layout expandable" ondblclick="enablescroll(\'' . $recordlayoutname . '\')" id="' . $recordlayoutname . '">';
						echo '<table><thead>';
						echo '<tr><td>Field Name (default value)</td><td>Data Alias</td><td>Data Align</td><td>Length</td><td>Position</td></tr>';
						echo '</thead><tbody>';
						
						$iPosition = 1;
						foreach($rows as $expression) {
							
							$fieldname = $expression['fieldname'];
							if (array_key_exists('defaultval', $expression)) {
								$fieldname .= " <b>(" . $expression['defaultval'] . ")</b>";
							} 
							
							echo '<tr>';
							echo '<td>' . $fieldname . '</td>';
							echo '<td>' . $expression['dataalias'] . '</td>';
							echo '<td>' . $expression['dataalign'] . '</td>';
							echo '<td>' . $expression['datalength'] . '</td>';
							echo '<td>' . $iPosition . '</td>';
							//echo '<td>'. $fieldexpression . '</td>';
							echo '</tr>';	
							if (is_numeric($expression['datalength']) ){
								$iPosition += $expression['datalength'];
							}
						}
						
						echo '<tbody>';
						echo '</table></div>';
						
						/* 
						 * Display RecordLayout specific Events
						 */
						foreach($aRecordLayoutEvents as $aRecordLayoutEvent) {
							//Display if current record layout matches with the above displayed record
							if (strtolower($aRecordLayoutEvent['name']) == strtolower($recordlayoutname)) {
								foreach($aRecordLayoutEvent['events'] as $aEvent) {
								echo 'RecordLayout Event: <span class="event-name">' . $aEvent['name'] . '</span><br />';
								
									echo '<div class="parameter-row">';
									echo '<table><thead>';
									echo '<tr><td colspan="3">Action Name</td></tr>';
									echo '<tr><td>Position</td><td>Name</td><td>Value</td></tr>';
									echo '</thead><tbody>';
									//echo '</tr></thead><tbody>';
									foreach($aEvent['actions'] as $aAction) {
										echo '<tr class="action-row"><td colspan="3"> ' . $aAction['name'] . '</td></tr>';
// 										echo '<h3>Parameters</h3>';
										foreach($aAction['parameters'] as $aParameter) {
											echo '<tr>';
											echo '<td>' . $aParameter['position'] . '</td>';
											echo '<td>' . $aParameter['name'] . '</td>';
											//Appending 'id' if the layout name is 'unknown' (to fix javascript errors)
// 											if (strtolower($aParameter['value']) == 'unknown') {
// 												$thisid = 'id' . $aParameter['value'];
// 											} else {
// 												$thisid = $aParameter['value'];
// 											}
											$thisid = 'target_' . (String) $aParameter['value'];
											if ($aParameter['name'] == 'record layout') {
												echo '<td><a href="#' . $thisid . '"  onclick="return getLastPos()">' . $aParameter['value'] . '</a></td>';
											} else if ($aParameter['name'] == 'expression' | $aParameter['name'] == 'count') {
												echo '<td><pre><code class="prettyprint lang-vb">' . $aParameter['value'] . '</pre></code></td>';
											} else {
												echo '<td>' . $aParameter['value'] . '</td>';
											}
											echo '</tr>';
											//echo '</tr></tbody></table></div>';
										}
									}
									echo '</tbody></table></div>';									
								}
							}
						}
					}
						
				} else {
						echo '<span class="missing-tag">No Record Layouts </span><br />';
				}
			}	
				
		}
		
		
		echo '<br /><hr>';
		
		$MapTargets = $xml->MapTargets;
		echo '<span id="header_maptargets"></span>';
		if (!$MapTargets) {
			echo '<br /><span class="missing-tag">No MapTargets tag found</span><br />';
//RenderClass::isMapTargetsTagExist			
		} else {
			
			echo '<br /><h1>MapTargets</h1>';
			foreach($MapTargets->MapTarget as $maptarget){
				echo '<h2>' . $maptarget['name'] . '</h2>';
				
				$mapschema = $maptarget->MapSchema;
				$schema = $mapschema->Schema;

				$aTargetSchemaInfo = array();
				if ($schema['connectorname']) { $aTargetSchemaInfo['connectorname'] = $schema['connectorname']; }
				if ($schema['designedfor'])   { $aTargetSchemaInfo['designedfor'] = $schema['designedfor']; }
				if ($schema['discriminatorrecord']) { $aTargetSchemaInfo['discriminatorrecord'] = $schema['discriminatorrecord']; }
				if ($schema['discriminatorfield']) { $aTargetSchemaInfo['discriminatorfield'] = $schema['discriminatorfield']; }
				
				echo '<br /><ul>';
				echo '<li><h2>Target Schema information</h2>'; echo '</li>';
				echo "<li><b>Connector Name: </b>" . $aTargetSchemaInfo['connectorname']; echo '</li>';
				echo "<li><b>Designed For: </b>" . $aTargetSchemaInfo['designedfor']; echo '</li>';
				if (array_key_exists('discriminatorrecord', $aTargetSchemaInfo)) {
					echo "<li><b>Discriminator Record: </b>" . $aTargetSchemaInfo['discriminatorrecord']; echo '</li>';
				} if (array_key_exists('discriminatorfield', $aTargetSchemaInfo)) {
					echo "<li><b>Discr. Field: </b>" . $aTargetSchemaInfo['discriminatorfield']; echo '</li>';
				}
				echo '</ul><br /><br />';
				
				
				$allfields = array();
				foreach($mapschema->MapExpressions->MapExpression as $expression) {
					$hashKey = hash("md5", $expression['recordlayoutname'] . '-' . $expression['fieldname']);
					$allfields[$hashKey] = $expression;
				}
				
				foreach($schema->RecordLayouts->RecordLayout as $layout) {
					$recordlayoutname = $layout['name'];
					//echo '<br /><h1>LayoutName: ' . $layout['name'] . '</h1>';
					
					$rows = array();
					foreach($layout->Fields->Field as $field) {
						$row = array();
						$row['layoutname'] = $layout['name'];
						$row['fieldname'] = $field['name'];
						$row['datatype'] = $field->Datatype['datatype'];
						$row['dataalias'] = $field->Datatype['dataalias'];
						$row['dataalign'] = $field->Datatype['dataalign'];
						$row['datalength'] = $field->Datatype['datalength'];
						$row['dataidentify'] = $field->Datatype['dataidentify'];
						$row['dataautoinc'] = $field->Datatype['dataautoinc'];
						$row['datacurrency'] = $field->Datatype['datacurrency'];
						$row['datarowversion'] = $field->Datatype['datarowversion'];
						$row['datapadchar'] = $field->Datatype['datapadchar'];
						try {
							if ($field->FieldOptions) {
								foreach ($field->FieldOptions->Option as $option){
									// 										echo (String) $option['name'];
									if ((String) $option['name'] == 'defaultexpression') {
										$row['defaultval'] = (String) $option['value'];
										// 											echo $row['defaultval'];
									}
								}
							}
						} catch (Exception $e) {
							echo 'Error while reading default value of a Source Record Layout ['.$recordlayoutname.'] field: ' . $e->getMessage();
						}						
						array_push($rows, $row);
					}


					
					echo '<span class="um-panel-header">';
					echo '<span class="um-panel-title">';
					echo 'Target RecordLayout: ' . $recordlayoutname;
					echo '</span><span class="um-panel-title-right" onclick="javascript:gotoLastPost()">[Return]</span>';
					echo '</span><br />';
					
					//Appending 'id' if the layout name is 'unknown' (to fix javascript errors)
// 					if (strtolower($recordlayoutname) == 'unknown') {
// 						$thisid = 'id' . $recordlayoutname;
// 					} else {
						$thisid = 'target_' . $recordlayoutname;
// 					}
					
					echo '<div class="target-layout expandable"  ondblclick="enablescroll(\'' . $thisid . '\')" id="' . $thisid . '">';
					echo '<table><thead>';
					echo '<tr><td>Field Name (default value)</td><td>Data Alias</td><td>Data Align</td><td>Length</td><td>Expression</td></tr>';
					echo '</thead><tbody>';
					foreach($rows as $expression) {
						$hashKey = hash("md5", $expression['layoutname'] . '-' . $expression['fieldname']);
						if (array_key_exists($hashKey, $allfields)) {
							$fieldexpression = '<pre><code class="prettyprint lang-vb expression-code">' . $allfields[$hashKey] . '</code></pre>' ;
						} else {
							$fieldexpression = '<span class="missing-tag">No Expression defined</span>';
						}
						
						$fieldname = $expression['fieldname'];
						if (array_key_exists('defaultval', $expression)) {
							$fieldname .= " <b>(" . $expression['defaultval'] . ")</b>";
						} 
						
						echo '<tr>';
						echo '<td>' . $fieldname . '</td>';
						echo '<td>' . $expression['dataalias'] . '</td>';
						echo '<td>' . $expression['dataalign'] . '</td>';
						echo '<td>' . $expression['datalength'] . '</td>';
						echo '<td>'. $fieldexpression . '</td>';
						echo '</tr>';	
					}
					echo '<tbody>';
					echo '</table></div>';
					
				}
			}
		}
	}
?>