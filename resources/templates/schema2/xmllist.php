<?php
	include_once ('../../config.php'); 
	$OnlyFileName = '';
	$files = glob(XML_Source_Path_Schema2 . '/*.{xml,XML}', GLOB_BRACE);
	$schemaPath = '/resources/templates/schema2';	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="/public_html/css/jquery-ui.css">
	<script src="/public_html/js/jquery.min.js"></script>
	<script src="/public_html/js/jquery-ui.js"></script>
	<script src="http://malsup.github.io/jquery.blockUI.js"></script>
	<link rel="stylesheet" href="/public_html/css/xmlliststyles.css">
</head>
<body>

<a href="/?schema=1" target="_top">Switch to Pervasive Schema</a>
<div id="container">
	<div id="masterbox">
		<div id="content">
			<div id="accordion-resizer" class="ui-widget-content">
				<div id="accordion">
				<h3 id="idCustSearch">Custom Search</h3>
				   <div class="Search-form" style="padding: 0px">
					   <label for="tags">Search:</label>
					   <input id="tags" style="width: auto;font-size: small;" placeholder="Input text to search">
					    <div style="overflow:auto; height: 300px;">
						  <ul class="filterlist" style="padding-left: 0px;font-size:x-small;">
							<?php 
							foreach($files as $file) {
								$OnlyFileName = str_replace(XML_Source_Path_Schema2 . '/', '', $file);
								echo '<li><a class="xml-ref" href="'.$schemaPath.'/main.php?xml=' . $OnlyFileName . 
									'" target="main" ><span>' . $OnlyFileName . 
									'</span></a></li>';
							}
							?>
						  </ul>
						  </div>
				    </div>
				<h3>All Maps</h3>
					<div>
						<?php 
						foreach($files as $file) {
							//do your work here
							$OnlyFileName = str_replace(XML_Source_Path_Schema2 . '/', '', $file);
							echo '<p><a class="xml-ref"  href="'.$schemaPath.'/main.php?xml=' . $OnlyFileName . '" target="main" >' . $OnlyFileName . '</a></p>';
						
						}?>
					</div>
				</div>
			</div>
			<div class="bufferRefreshDiv"  style="display:none;">
			<a target="main" href="<?php echo $schemaPath;?>/main.php?xml=<?php echo $OnlyFileName; ?>&buffer=2" class="bufferRefresh">Regenerate This Page</a>
			</div>
			<div class="growlUI" style="display:none;font-size:x-large">Buffered Page deleted. Please refresh!!</div>
<!-- 			
			<div class="indexes"></div>
			
			<div>
			<a target="main" href="status.php" class="button-link">Check Status</a>
			</div>
			<div class="download-div">
			<a target="_blank" href="download-headers.php?xml=<?php echo $OnlyFileName; ?>" class="button-link download-link">Download Layour Fields</a>
			</div>
 -->			
		</div><!--end content-->
	</div><!--end masterbox-->
</div><!--end container-->	

	
<script type="text/javascript">
  
  $.expr[":"].contains = $.expr.createPseudo(function(arg) {
	    return function( elem ) {
	        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
	    };
  });

  $(function() {
	  
	    $( "#accordion" ).accordion({
	        heightStyle: "fill",
//	         heightStyle: "content",
	        collapsible: false,
	        active: 1
	      });
	  
		/*display tags even for detault page on load*/
		$(".map-index").remove();
        $("div.indexes").append('<div class="map-index"></div>');
		appendTags('/src/main.php?xml=<?php echo DEFAULT_XML_FILE ?>');

		/*display download header link even for detault page on load*/
		$(".download-link").remove();
        $("div.download-div").append('<a target="_blank" href="download-headers.php?xml=<?php echo DEFAULT_XML_FILE ?>" class="button-link download-link">Download Layout Fields</a><a target="_blank" href="informatica/informatica.php?xml=<?php echo DEFAULT_XML_FILE ?>" class="button-link download-link">Download Informatica Layout</a>');
		//appendTags('/src/main.php?xml=<?php echo DEFAULT_XML_FILE ?>');
		
       // $("a.bufferRefresh").remove();
//        $("div.bufferRefreshDiv").append('<a target="main" href="<?php echo $schemaPath;?>/main.php?xml=<?php echo DEFAULT_XML_FILE ?>&buffer=2" class="button-link bufferRefresh">Regenerate This Page</a>');
	
		
	   $( ".filterlist li" ).hide();
	
	   $( "#tags" ).keyup(function() {
		   $( ".filterlist li" ).hide();
			var i = 0;
		   if (($(this).val().length) > 1) {
			   
			   $( ".filterlist li a span:contains('" + $(this).val() + "')" ).each(function(){
					i = i + 1;
				   	$(this).parent().parent().show();
				}); 

				$('#idCustSearch').html("Custom Search (" + i + ")");
				
		   } else {
			   $('#idCustSearch').html("Custom Search");
		   }
	   });
		  
	    $( "#accordion-resizer" ).resizable({
	      minHeight: 800,
	      minWidth: 80,
	      resize: function() {
	        $( "#accordion" ).accordion( "refresh" );
	      }
	    });
	
	    $( "a.xml-ref" ).click(function() {
	        var page = $(this).attr('href');
	
	        $(".map-index").remove();
	        $("div.indexes").append('<div class="map-index"></div>');
	        appendTags(page);

			var file = page.split("=");
			$(".download-link").remove();
	        $("div.download-div").append('<a target="_blank" href="download-headers.php?xml=' + file[1] + '"  class="button-link download-link">Download Layout Fields</a>');

	        $("a.bufferRefresh").remove();
	        $("div.bufferRefreshDiv").append('<a target="main" href="<?php echo $schemaPath;?>/main.php?xml=' + file[1] + '&buffer=2" class="button-link bufferRefresh">Regenerate This Page</a>');
 			$("div.bufferRefreshDiv").attr('style', 'display:block;');
	
	    }); 

	    $(document).on( "click", "a.bufferRefresh", function() {
	    	//$.blockUI({ message: '<h1><img src="busy.gif" /> Just a moment...</h1>' });
	    	//$.blockUI({ message: $('#domMessage') });
	    	$.blockUI({ 
	            message: $('div.growlUI'), 
	            fadeIn: 700, 
	            fadeOut: 700, 
	            timeout: 2500, 
	            showOverlay: false, 
	            centerY: false, 
	            css: { 
	                width: '350px', 
	                top: '10px', 
	                left: '', 
	                right: '10px', 
	                border: 'none', 
	                padding: '5px', 
	                backgroundColor: '#F00', 
	                '-webkit-border-radius': '10px', 
	                '-moz-border-radius': '10px', 
	                opacity: .8, 
	                color: '#fff' 
	            } 
	        }); 
	    });

  });

	  function appendTags(page) {
	      var tag = '<ul>';
	  	  tag += '<li><a href="' + page + '#header_mapdescription' + '" target="main">Map Description</a></li>';
	      tag += '<li><a href="' + page + '#header_mapvariables' + '" target="main">Map Variables</a></li>';
	      tag += '<li><a href="' + page + '#header_mapevents' + '" target="main">Map Events</a></li>';
	      tag += '<li><a href="' + page + '#header_mapsources' + '" target="main">Map Sources</a></li>';
	      tag += '<li style="list-style-type:none"><ul><li><a href="' + page + '#header_sourceevents' + '" target="main">Source Events</a></li></ul></li>';
	      tag += '<li><a href="' + page + '#header_maptargets' + '" target="main">Map Targets</a></li>';
	      tag += '</ul>';
	      $(".map-index").append(tag);
	  }
</script>	

<br />
<br />
<br />
<br />
<footer>
(c) Powered by Syntel Inc.,
</footer>
</body>
</html>