<?php
include_once '../../classes/ArchiveClass.php';

function render_xml_data($path_to_xml_file){
		
		$paramIndex = 1;  //used for param unique ids
		
		if (!file_exists($path_to_xml_file)){
			return;
		}else{
			$chars_to_replace = array('[\r]','[\t]');
			try {
				$xmlstring = trim(preg_replace($chars_to_replace, '', file_get_contents($path_to_xml_file, true)));
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
		$xml = new SimpleXMLElement($xmlstring);
		
		$zipObj = new ArchiveClass(XML_TITLE_PLAIN);
			
		
		
		echo '<br /><hr>';
		
//RenderClass::Class __construct()
		$Repo = $xml->REPOSITORY;
		echo '<span id="header_mapRepository"></span>';
		if(!$Repo) {
			echo '<br /><span class="missing-tag">No Repository tag found</span><br />';
			unset($Repo);
		}else {
			if($Repo['NAME']){
				echo 'Repository: ' . $Repo['NAME'] . '<br /><hr>';
			}
			
			$Folder = $Repo->FOLDER;
			if (!$Folder){
				echo '<br /><span class="missing-tag">No Repository tag found</span><br />';
				unset($Folder);
			} else {
				if ($Folder['NAME']){
					echo 'Folder: ' . $Folder['NAME'] . '<br /><hr>';
				}
				
				
				
				
				$Mappings = $xml->xpath('/POWERMART/REPOSITORY/FOLDER/MAPPING');
				if ($Mappings){
					$Mapping = $Mappings[0];
					echo 'Mapping Name: ' . (String) $Mapping['NAME'];
				}
				
				
				echo '<input type="button" class="ShowPageButton" value="Show Sample Image"/>';
				echo '<img src="/public_html/img/content/Map_wf_m_101063_C_CTS_V01.png" style="display:none;" class="ShowPageImage" />';				
				
				
				$Variables = $xml->xpath('/POWERMART/REPOSITORY/FOLDER/WORKFLOW/WORKFLOWVARIABLE[@USERDEFINED="YES"]');
				if ($Variables){
					echo '<div class="parameter-row">';
					echo '<table><thead>';
					echo '<tr><td colspan="3">User Defined Vairable(s): '. count($Variables) . '</td></tr>';
					echo '<tr><td>NAME</td><td>DATATYPE</td><td>ISNULL</td><td>ISPERSISTENT</td><td>DEFAULTVALUE</td></tr>';
					echo '</thead><tbody>';
					foreach($Variables as $Variable){
						echo '<tr>';
						echo '<td>' .  $Variable['NAME'] . '</td>';
						echo '<td>' .  $Variable['DATATYPE'] . '</td>';
						echo '<td>' .  $Variable['ISNULL'] . '</td>';
						echo '<td>' .  $Variable['ISPERSISTENT'] . '</td>';
						echo '<td>' .  $Variable['DEFAULTVALUE'] . '</td>';
						echo '</tr>';
					}
					echo '</tbody></table></div>';
				}
				
				//Read 'FLATFILE' attributes
				//Read 'TABLEATTRIBUTE' attributes
				
				$Elements = $xml->xpath('/POWERMART/REPOSITORY/FOLDER/SOURCE');
				ConvertTagstoTable($Elements,  array('NAME', 'DBNAME'), 'SOURCEFIELD', array('NAME', 'PHYSICALLENGTH', 'PRECISION'));

				$Elements = $xml->xpath('/POWERMART/REPOSITORY/FOLDER/TARGET');
				ConvertTagstoTable($Elements, array('NAME', 'DATABASETYPE'), 'TARGETFIELD', array('NAME', 'PHYSICALLENGTH', 'PRECISION'));
				
				$Elements = $xml->xpath('/POWERMART/REPOSITORY/FOLDER/MAPPING/TRANSFORMATION');
				ConvertTagstoTable($Elements, array('NAME', 'TYPE'), 'TRANSFORMFIELD', array('NAME', 'DATATYPE', 'PRECISION'), 'EXPRESSION');
				
				
// 				<INSTANCE DESCRIPTION ="" 
// 						NAME ="SQ_INPUT_Ceridian_C_CERI_Yes_EZstart_No_EZincrease_FormattedInput" 
// 								REUSABLE ="NO" 
// 										TRANSFORMATION_NAME ="SQ_INPUT_Ceridian_C_CERI_Yes_EZstart_No_EZincrease_FormattedInput" 
// 												TRANSFORMATION_TYPE ="Source Qualifier" TYPE ="TRANSFORMATION">
// 					<ASSOCIATED_SOURCE_INSTANCE NAME ="INPUT_Ceridian_C_CERI_Yes_EZstart_No_EZincrease_FormattedInput"/>
// 				</INSTANCE>
				$Instances = $xml->xpath('/POWERMART/REPOSITORY/FOLDER/MAPPING/INSTANCE');
				$parentfields = array('TRANSFORMATION_NAME', 'TRANSFORMATION_TYPE', 'TYPE');
				$aRecordLayoutEvents = array();
				
				foreach($Instances as $Source) {
					$aRecordLayoutEvent = array();
					$index = 0;
					foreach ($parentfields as $parentfield){
						$aRecordLayoutEvent[$index] = (String) $Source[$parentfield];
						$index = $index + 1;
					}
					array_push($aRecordLayoutEvents, $aRecordLayoutEvent);
				}
				
				
// 				echo var_export($aRecordLayoutEvents, TRUE);
// 				<CONNECTOR FROMFIELD ="Employee_Address_Line2"
// 						FROMINSTANCE ="JNR_EMP_REC_1_2_CON_LOAN"
// 						FROMINSTANCETYPE ="Joiner"
// 						TOFIELD ="Employee_Address_Line2"
// 						TOINSTANCE ="EXP_FORMAT"
// 						TOINSTANCETYPE ="Expression"/>
				
				$Connectors = $xml->xpath('/POWERMART/REPOSITORY/FOLDER/MAPPING/CONNECTOR');
// 				if (!function_exists('getConnectionArrayBetween')) {
// 					echo 'function exists';
// 					die;
// 				}
// 				$expConections = getConnectionArrayBetween($Connectors, 'Exp_Trim', 'OUTPUT_101063_C_CTS_V01');
				//echo var_export($Connectors);
// 				die;
				$fromInstance = 'SRT_SSN_FLAG';
				$toInstance = 'Exp_TrimCommas';
				$aConnections = array();
				foreach($Connectors as $connection) {
				
					$thisFromInstance = (String) $connection['FROMINSTANCE'];
					$thisFromField = (String) $connection['FROMFIELD'];
					$thisToInstance = (String) $connection['TOINSTANCE'];
					$thisToField = (String) $connection['TOFIELD'];
				
// 					echo $fromInstance . '==' . $thisFromInstance . '&&' . $toInstance . '==' . $thisToInstance . '<br>';
// 					if ($fromInstance == $thisFromInstance && $toInstance == $thisToInstance) {
						$thisConnection = array();
						$thisConnection[0] = $thisFromInstance; //.'_'.$thisFromField;
						$thisConnection[1] = $thisToInstance; //.'_'.$thisToField;
						$thisConnection[2] = $thisFromField.'->'.$thisToField;
						//$thisConnection[3] = $thisToField;
						
						array_push($aConnections, $thisConnection);
						unset($thisConnection);
// 					}
				
				}
				
				$parentfields = array('FROMINSTANCE', 'TOINSTANCE');
				$aRecordLayoutEvents = array();
				
				
				
				$SourceTags = array();
				$index = 0;
				foreach($Connectors as $Source) {
					
					$x = (String) $Source['FROMINSTANCE'];
					$y = (String) $Source['TOINSTANCE'];
					
					//Handle SD and SQ expressions
					if ($x == $y){
						array_push($SourceTags, $x);
						if ( (String) $Source['FROMINSTANCETYPE'] == "Source Definition"){ $x = $x . '_SD'; }
						if ( (String) $Source['FROMINSTANCETYPE'] == "Source Qualifier" ){ $x = $x . '_SQ'; } 
							
						if ( (String) $Source['TOINSTANCETYPE'] == "Source Definition"){ $y = $y . '_SD'; }
						if ( (String) $Source['TOINSTANCETYPE'] == "Source Qualifier" ){ $y = $y . '_SQ'; }
					}					
					
					$bExists = isAlreadyExists($aRecordLayoutEvents, $x, $y);
					
					if (!$bExists){
						$aRecordLayoutEvent = array();
						$index = 0;
						//foreach ($parentfields as $parentfield){
							$aRecordLayoutEvent[0] = $x;
							$aRecordLayoutEvent[1] = $y;
							$index = $index + 1;
						//}
						array_push($aRecordLayoutEvents, $aRecordLayoutEvent);
					}
				}	
				

				
				
				$TreeSources = array();
				$TreeTargets = array();
				$TreeConnections = array();
				
				foreach ($aRecordLayoutEvents as $aRecordLayoutEvent){
					$x = $aRecordLayoutEvent[0];
					$y = $aRecordLayoutEvent[1];
					
					foreach($SourceTags as $SourceTag){
						if($x == $SourceTag){
							$x = $x . '_SQ';
						}
					}
					
					$TreeConnection = array();
					$TreeConnection[0] = $x;
					$TreeConnection[1] = $y;
					array_push($TreeConnections, $TreeConnection);
				}
// 				foreach($TreeConnections as $TreeConnection){
// 					echo $index++ . ') ' . $TreeConnection[0] . ' -> ' . $TreeConnection[1] . '<br />';
// 				}

			
				
?>

			   <script type="text/javascript">
			      google.setOnLoadCallback(drawChart);
			
			      function drawChart() {
			        var data = new google.visualization.DataTable();
			        data.addColumn('string', 'From');
			        data.addColumn('string', 'To');
			        data.addColumn('number', 'Weight');
// 			        data.addColumn({type: 'string', role: 'tooltip'});
			     // A column for custom tooltip content
			        data.addRows([<?php 
// 			          [ 'A', 'X', 5 ],
// 			          [ 'A', 'Y', 7 ],
// 			          [ 'A', 'Z', 6 ],
// 			          [ 'B', 'X', 2 ],
// 			          [ 'B', 'Y', 9 ],
// 			          [ 'B', 'Z', 4 ]
				        foreach($TreeConnections as $TreeConnection){
				        	echo "\n['" . $TreeConnection[0] . "', '" . $TreeConnection[1] . "', 1 ],";
				        	//echo "\n['" . $TreeConnection[0] . "', '" . $TreeConnection[1] . "', 1, '" . $TreeConnection[2] . "'],";
				        }
			        
			          ?>
			        ]);
					<?php
						$cnt = count($aConnections);
						$width = 1000 + (20 * intval($cnt / 10));
						//$width = 600; 
						$height = 100 + (5 * intval($cnt / 10));
						defined('Sanky_Window_Heigth') or define('Sanky_Window_Heigth', $height + 30);
					?>
			        // Sets chart options.
			        var colors = ['#a6cee3', '#b2df8a', '#fb9a99', '#fdbf6f',
                  '#cab2d6', '#ffff99', '#1f78b4', '#33a02c'];
	                  
			        var options = {
// 			          tooltip: {isHtml: true},
			          width: <?php echo $width ?>,
			          height: <?php echo $height ?>,
			          sankey: {
// 			              node: { colors: colors, 
// 				              label: { fontName: 'Times-Roman',
// 			                               fontSize: 12,
// 			                               color: '#871b47',
// 			                               bold: true,
// 			                               italic: true },
// 			                      labelPadding: 20,
// 			                      //nodePadding: 10
// 			                         },
// 			                         link: {
// 			                             //colorMode: 'gradient',
// 			                             colors: colors
// 			                           } 
						},
			      			          
			        };
			
			        // Instantiates and draws our chart, passing in some options.
			        var chart = new google.visualization.Sankey(document.getElementById('sankey_multiple'));
			        if (chart) {
			            chart.clearChart();
			        }
			        chart.draw(data, options);

			        google.visualization.events.addListener(chart, 'select', selectHandler);

			        function selectHandler(e) {
			          alert('A table row was selected');
			        }
			      }
			    </script>
			   

<?php 				
				
				
				

				/*
				 * Display RecordLayout specific Events
				 */
// 				foreach($aRecordLayoutEvents as $aRecordLayoutEvent) {
// 					echo '<div class="parameter-row">';
// 					echo '<table><thead>';
// 					echo '<tr><td colspan="3">Source Name:   '. $aRecordLayoutEvent[0] . '[' . $aRecordLayoutEvent[1] . ']['.$aRecordLayoutEvent[2]. ']</td></tr>';
// 					echo '<tr>';
// // 					foreach ($childfields as $childfield){
// // 						echo '<td>' . $childfield . '</td>';
// // 					}
// 					echo '</tr>';
// 					echo '</thead><tbody>';
// 					echo '</tbody></table></div>';
// 				}
				
				
			}
			
		}
			
	}
	
	function getConnectionArrayBetween($connectors = null, $fromInstance = null, $toInstance = null) {
		/**
		 * echo var_export($Connectors);
		 * array ( 'FROMFIELD' => 'O_Data', 'FROMINSTANCE' => 'Exp_Trim', 'FROMINSTANCETYPE' => 'Expression',
		 *         'TOFIELD' => 'Data', 'TOINSTANCE' => 'OUTPUT_101063_C_CTS_V01', 'TOINSTANCETYPE' => 'Target Definition', )
		 */
		$aConnections = array();
		foreach($connectors as $connection) {
				
			$thisFromInstance = (String) $connection['FROMINSTANCE'];
			$thisFromField = (String) $connection['FROMFIELD'];
			$thisToInstance = (String) $connection['TOINSTANCE'];
			$thisToField = (String) $connection['TOFIELD'];
				
			if ($fromInstance == $thisFromInstance && $toInstance == $thisToInstance) {
				$thisConnection = array();
				$thisConnection[0] = $thisFromField;
				$thisConnection[1] = $thisToField;
				array_push($aConnections, $thisConnection);
				unset($thisConnection);
			}
	
		}
		return $aConnections;
	}
	
?>
