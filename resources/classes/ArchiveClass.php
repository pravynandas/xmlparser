<?php
class ArchiveClass {
	
	private $zip;
	public $status;
	public $error;
	
	function __construct($filename){
		$this->zip = new ZipArchive();
		$zipfilename = '../headers/' . $filename . '.zip';
		
		if ($this->zip->open($zipfilename, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)) {
			$this->status = true;
// 			printf('Failed with code %d', $ret);
		} else {
			$this->status = false;
		}
	}
	
	
	function addFile($filename, $contents) {

		try {
			$this->zip->addFromString($filename, $contents);
// 			$ret = $zip->open($zipfilename, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
// 			if ($ret !== TRUE) {
// 				printf('Failed with code %d', $ret);
// 			} else {
// 				$options = array('add_path' => '../headers/temp/', 'remove_all_path' => TRUE);
// 				$zip->addGlob('*.{dat}', GLOB_BRACE, $options);
// 				$zip->close();
// 			}
			// try {
			// 	$Zip->open($zipfilename, ZipArchive::OVERWRITE);
			// 	$Zip->addEmptyDir('Record Layout Headers');
			// 	$Zip->addGlob('../headers/temp/*.{dat}');
			// } catch (Exception $e) {
			// 	echo $e->getMessage();
			// }
			// echo "Extra:" . $zipfilename;
			// 			$files = glob('../headers/temp/*.{dat}', GLOB_BRACE);
			// 			foreach($files as $file) {
			// // 				$filename = str_replace(realpath('../headers/'), '', realpath($file));
			// 				$filename = realpath($file);
			// // 				echo $filename . "\n" ;
			// // 				$Zip->addFile($filename);
			// 			}
			// 			$Zip->close();
		} catch (Exception $e) {
			$this->error .= "\n" . $filename . "\t" . $e->getMessage();
		}
		
	}
	
	function __destruct() {
		$this->zip->close();
	}
}