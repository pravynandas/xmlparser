<?php
// namespace help;
// class HelperClass{

/**
 * Below code will buffer existing HTML file; bufferes if one does not exist or xml file is newer.
*/
function check_buffer_html($xmlfile) {

	$schemaXmlPath = XML_FILE_PATH . '\\' . GLOBAL_SCHEMA_IDENTIFIER;
	$currentxmlfile = $schemaXmlPath . '\\' . $xmlfile;
	
	
			if (!defined('XML_FILE_FULL_PATH')) {
				define('XML_FILE_FULL_PATH', $currentxmlfile);
			}

			
			
			if (!file_exists($currentxmlfile)){
				echo 'file [' . $currentxmlfile . '] not found. Action aborted';
				return false;
			}
			
	$timestamp = filemtime($currentxmlfile);
	
			if (!defined('XML_MODIFIED_TIME_STAMP')) {
				define('XML_MODIFIED_TIME_STAMP', $timestamp);	
			}
	
	$pagetitlePlain = str_replace($schemaXmlPath . '\\', '', str_replace('all/', '', $currentxmlfile));
	
	
			if (!defined('XML_TITLE_PLAIN')) {
				define('XML_TITLE_PLAIN', $pagetitlePlain);
			}
	
	$bufferedhtmlurl = '/public_html/xml/' . GLOBAL_SCHEMA_IDENTIFIER . '/buffered/' . $pagetitlePlain . '_' . $timestamp . '.html';
	$bufferedhtmlfile =  realpath($schemaXmlPath . '/buffered/') . '\\' . $pagetitlePlain . '_' . $timestamp . '.html';
	//$bufferedhtmlfile = $bufferedxmlfile . '.html';
			if (!defined('BUFFER_HTML_FILE_NAME')) {
				define('BUFFER_HTML_FILE_NAME', $bufferedhtmlfile);
			}
			
// 			echo $currentxmlfile. '<br>';
// 			echo $timestamp. '<br>';
// 			echo $pagetitlePlain . '<br>';
// 			echo $bufferedhtmlurl . '<br>';
// 			echo $bufferedhtmlfile . '<br>';
// 			echo (file_exists($bufferedhtmlfile)) ? 'exists' : 'nope' ;
// 			die;
	
	
	//echo $bufferedhtmlfile;	die;
	//check if an existing html file exists
	if (file_exists($bufferedhtmlfile)) {

// 		try {
// 			$bufferedHTMLfileContents = file_get_contents($bufferedhtmlfile);
// 		} catch (Exception $e) {
// 			echo $e->getMessage();
// 		}
		
// 		if ($bufferedHTMLfileContents) {
// 			//echo $bufferedHTMLfileContents;
// 			return true;
// 		}

//$path = str_replace($bufferedhtmlfile, '', $path)
// 	echo $bufferedhtmlurl;
// 	die;
	try {
		header('Location: '. $bufferedhtmlurl);
	} catch (Exception $e) {
		echo 'error while redirecting to buffer page:' . $e->getMessage();
	}
		return true;
		
	}
	
	return false;
}


function ConvertTagstoTable($items, $parentfields, $childnode, $childfields, $optionalfield = null){
	
	if($optionalfield && array_key_exists('optional', $childfields)){
		echo 'Sorry, \'optional\' is a reserved array element while an optional field param is present';
		return true;
	}
	
	$aRecordLayoutEvents = array();
	foreach($items as $Source) {
			
		$aRecordLayoutEvent = array();
		$index = 0;
		foreach ($parentfields as $parentfield){
			$aRecordLayoutEvent[$index] = $Source[$parentfield];
			$index = $index + 1;
		}
		$aRecordLayoutEvent['optional'] = false;
			
			
		$aActions = array();
		$aEvents = array();
		
		foreach($Source->xpath('./' . $childnode) as $action) {

			$aAction = array();
			$aParameters = array();
			$aParameter = array();
			
			$cindex = 0;
			foreach ($childfields as $childfield){
				$aParameter[$cindex] = (String) $action[$childfield];
				$cindex = $cindex + 1;
			}
			
			if ($optionalfield){
				if ($action[$optionalfield]){
					$aRecordLayoutEvent['optional'] = true;
					$aParameter[$optionalfield] = (String) $action[$optionalfield];
				}else{
					$aParameter[$optionalfield] = '';
				}
			}
				
			array_push($aParameters, $aParameter);
			
			$aAction['parameters'] = $aParameters;
			array_push($aActions, $aAction);
		}
			
		$aRecordLayoutEvent['events'] = $aActions;
		array_push($aRecordLayoutEvents, $aRecordLayoutEvent);
	}
	
	/*
	 * Display RecordLayout specific Events
	 */
	foreach($aRecordLayoutEvents as $aRecordLayoutEvent) {
		echo '<div class="parameter-row">';
		echo '<table><thead>';
		echo '<tr><td colspan="3">Source Name:   '. $aRecordLayoutEvent[0] . ']</td></tr>';
		echo '<tr>';
		foreach ($childfields as $childfield){
			echo '<td>' . $childfield . '</td>';
		}
		echo '</tr>';
		echo '</thead><tbody>';
		
		$bExpression = $aRecordLayoutEvent['optional'];
		
		foreach($aRecordLayoutEvent['events'] as $aAction) {
			foreach($aAction['parameters'] as $aParameter) {
				echo '<tr>';
				$index = 0;
				foreach ($childfields as $childfield){
					echo '<td>' . $aParameter[$index] . '</td>';
					$index = $index + 1;
				}
				
				if($bExpression){
					echo '<td>' . $aParameter[$optionalfield] . '</td>';
				}
				echo '</tr>';
			}
		}
		echo '</tbody></table></div>';
	}
	
}



function isAlreadyExists($items, $x, $y){
	foreach($items as $item){
		if($item[0] == $x && $item[1] == $y){
			return true;
		}
	}
	return false;
} 

function isSource($items, $x){
	$bFlag = false;
	foreach($items as $item){
		if($item[1] == $x){
			$bFlag = true;
		}
	}
	return !$bFlag;
}


function isTarget($items, $x){
	$bFlag = false;
	foreach($items as $item){
		if($item[0] == $x){
			$bFlag = true;
		}
	}
	return !$bFlag;
}



// }