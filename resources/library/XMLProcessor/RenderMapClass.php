<?php
include_once '../../config.php';

class RenderMapClass {

	public $xml; //xml doc object
	public $status;
	public $xmlpath; //set file path for reference
	public $error;
	public $chars_to_replace = array('[\r]','[\t]');
	public $MapEvents = array();       //array to hold ALL the mapevents
	public $MapSources = array();	   //array to hold ALL the mapsources and correcponding record layouts, incl. Rules
	public $MapTargets = array();      //array to hold ALL the maptargets and correcponding record layouts, incl. fields
	public $TargetFields = array(); 
	
	function __construct($path_to_xml_file) {
		if (!file_exists($path_to_xml_file)){
			$this->error = "xml file [". $path_to_xml_file .  realpath($path_to_xml_file) ."] does not exists.";
			$this->status = false;
			return;
		}else{
			try {
				$xmlstring = trim(preg_replace($this->chars_to_replace, '', file_get_contents($path_to_xml_file, true)));
			} catch (Exception $e) {
				$this->error = $e->getMessage();
			}
		}
		$this->xmlpath = $path_to_xml_file;
		
		try {
			$xml = new SimpleXMLElement($xmlstring);
		} catch (Exception $e) {
			$this->error .= $e->getMessage();
		}
		if ($xml) {
			$this->status = true;
			$this->xml = $xml;
		}
	}
	
	function __destruct(){
		if ($this->xml){
			$this->xml = null;
		}
	}
	
	public function isDescriptionTagExist() {
		$tag = $this->xml->xpath(XP_MAP_DESC);
		return (count($tag));
	}
	public function isVariablesTagExist() {
		$tag = $this->xml->xpath(XP_MAP_VARS);
		if ($tag) {
			return (count($tag[0]->Variable));
		} else {
			return 0;
		}
	}
	public function isMapEventsTagExist() {
		$tag = $this->xml->xpath(XP_MAP_MAPEVENTS);
		if ($tag) {
			return (count($tag[0]->Event));
		} else {
			return 0;
		}
	}
	public function isMapSourcesTagExist() {
		$tag = $this->xml->xpath(XP_MAP_MAPSOURCES);
		if ($tag) {
			return (count($tag[0]->MapSource));
		} else {
			return 0;
		}
	}
	public function isMapTargetsTagExist() {
		$tag = $this->xml->xpath(XP_MAP_MAPTARGETS);
		if ($tag) {
			return (count($tag[0]->MapTarget));
		} else {
			return 0;
		}
	}
	
	public function getDescriptionJSON(){
		//TODO: Build it
	}
	public function getVariablesJSON(){
		//TODO: Build it
	}
	
	public function getMapEventsArray() {
		if ($this->MapEvents = array(0)) {
			$this->processMapEvents();
		}
		return $this->MapEvents;
	}
	public function getMapEventsJSON() {
		if ($this->MapEvents = array(0)) {
			$this->processMapEvents();
		}
		return json_encode($this->MapEvents);
	}
	
	
	
	public function processMapEvents() {
		$MapEvents = $this->xml->MapEvents;
		$aMapEvents = array();
		if ($MapEvents) {
			$aEvents = array();
			foreach($MapEvents->Event as $Event){
				$eventname = (String) $Event['name'];
				
				$ThisEvent = array();
				$ThisEvent['name'] = $eventname;
				
				$aActions = array();
				foreach($Event->Action as $Action) {
					$ThisAction = array();
					$ThisAction['name'] = (String) $Action['name'];
					
					$aParams = array();
					foreach($Action->Parameter as $Param) {
						$ThisParam = array();
						$ThisParam['position'] = (String) $Param['position'];
						$ThisParam['name'] = (String) $Param['name'];
						$ThisParam['value'] = (String) $Param;
						array_push($aParams, $ThisParam);						
					}
					$ThisAction['params'] = $aParams;
					array_push($aActions, $ThisAction);
				}
				$ThisEvent['actions'] = $aActions;
				array_push($aEvents, $ThisEvent);
			}
			$aMapEvents = $aEvents;
		}
		$this->MapEvents = $aMapEvents;
	}
	
	public function getRecordLayoutEventsArray($recordlayoutname=null) {
		if ($this->MapSources = array(0)) {
			$this->processMapSources();
		}
		
		if ($recordlayoutname) {
			$aMapSourceLayouts = $this->MapSources;
			foreach($aMapSourceLayouts as $aMapSourceLayout) {
				foreach($aMapSourceLayout['layouts'] as $MapSourceLayout) {
					if ($MapSourceLayout['name'] == $recordlayoutname) {
						return $MapSourceLayout['events'];
					}
				}
			}
		} else {
			return array(0);
		}
	}

	
	public function getMapSourcesArray() {
		if ($this->MapSources = array(0)) {
			$this->processMapSources();
		}
		return $this->MapSources;
	}
	public function getMapSourcesJSON() {
		if ($this->MapSources = array(0)) {
			$this->processMapSources();
		}
		return json_encode($this->MapSources);
	}
	
	
	
	
	public function processMapSources() {
		$objMapSources = $this->xml->xpath(XP_MAP_MAPSOURCES);
		if ($objMapSources) {
				
			$aMapSources = array();
			foreach ($objMapSources[0]->MapSource as $MapSource) {
				$mapSourcename = (String) $MapSource['name'];
		
				$ThisMapSource = array();
				//ThisMapSource array will have the source name as Key and LayoutsArray as value
		
				//Get all the events; association will be made by layout name as primary key
				$aRecordLayoutEvents = $this->getLayoutEvents();
				
				$MapSchema = $MapSource->MapSchema;
				$Schema = $MapSchema->Schema;
				
				$aRules = array();
				if ($Schema->Rules) {
						
					foreach($Schema->Rules->Rule as $Rule) {
						$RulePriKey = strtolower((String) $Rule['recordname']);
						$hashKey = hash("md5", $RulePriKey);
						
						$ThisRule = array();
						$ThisRule['name'] = (String) $Rule['name'];
						$ThisRule['lowvalue'] = (String) $Rule['lowvalue'];
						$ThisRule['operator'] = (String) $Rule['operator'];
						$ThisRule['casesensitive'] = (String) $Rule['casesensitive'];
						$ThisRule['recordname'] = (String) $Rule['recordname'];
						
						$aRules[$hashKey] = $ThisRule;
					}
				}
		
				$aRecordLayouts = array();
				if ($Schema->RecordLayouts) {
						
					foreach($Schema->RecordLayouts->RecordLayout as $RecordLayout) {
						$recordlayoutname = (String) $RecordLayout['name'];
		
						$ThisRecordLayout = array();
						$ThisRecordLayout['name'] = $recordlayoutname;
		
						$SourceFields = array();
						if ($RecordLayout->Fields) {
								
							$Fields = $this->extractRecordLayouts($RecordLayout->Fields);
							$SourceFields = $Fields;
						}
						$ThisRecordLayout['fields'] = $SourceFields;
						
						//Now associate rules with it's corresponding layout (only for MapSources)
						$AssocRule = array();
						$RulePriKey = strtolower($recordlayoutname);
						$hashKey = hash("md5", $RulePriKey);
						if (array_key_exists($hashKey, $aRules)) {
							$AssocRule = $aRules[$hashKey];
						} 
						$ThisRecordLayout['rule'] = $AssocRule;
						
						//Now Associate the events specific to this layout
						$AssocEvents = array();
						$EventPriKey = strtolower($recordlayoutname);
						$hashKey = hash("md5", $EventPriKey);
						if (array_key_exists($hashKey, $aRecordLayoutEvents)) {
							$AssocEvents = $aRecordLayoutEvents[$hashKey];
						}
						$ThisRecordLayout['events'] = $AssocEvents;
						
						array_push($aRecordLayouts, $ThisRecordLayout);
					}
				}
				
				
				$ThisMapSource['layouts'] = $aRecordLayouts;
				$ThisMapSource['sourceevents'] = $this->getSourceEvents();
				
				$aMapSources[$mapSourcename] = $ThisMapSource;
			}
			
			
			$this->MapSources = $aMapSources;
		}		
	}	
	
		public function getLayoutEvents(){
			$RecordLayoutEvents = $this->xml->xpath(XP_MAP_MAPSOURCE_RECORDLAYOUTEVENTS);
			
			$aRecordLayoutEvents = array();
			foreach($RecordLayoutEvents as $recordlayoutevent) {
				$recordlayouteventname = (String) $recordlayoutevent['recordlayoutname'];
				
				$aEvents = array();
				foreach($recordlayoutevent->Event as $event) {
					$eventname = (String) $event['name'];
			
					$aActions = array();
					foreach($event->Action as $action) {
						$actionname = (String) $action['name'];
			
						$aParameters = array();
						foreach($action->Parameter as $parameter) {
							$aParameter = array();
							$aParameter['position'] = (String) $parameter['position'];
							$aParameter['name'] = (String) $parameter['name'];
							$aParameter['value'] = (String) $parameter;
							array_push($aParameters, $aParameter);
						}
						$aActions[$actionname] = $aParameters;
					}
					$aEvents[$eventname] = $aActions;
				}
				
				$hashKey = hash("md5", strtolower($recordlayouteventname));
				$aRecordLayoutEvents[$hashKey] = $aEvents;
			}
			
			return $aRecordLayoutEvents;
		}
		
		public function getSourceEvents(){
			$SourceEvents = $this->xml->xpath(XP_MAP_MAPSOURCE_SOURCEEVENTS);
			
			$aSourceEvents = array();
			foreach($SourceEvents as $SourceEvent) {
					
				$aEvents = array();
				foreach($SourceEvent->Event as $event) {
					$eventname = (String) $event['name'];
			
					$aActions = array();
					foreach($event->Action as $action) {
						$actionname = (String) $action['name'];
			
						$aParameters = array();
						foreach($action->Parameter as $parameter) {
							
							$aParameter = array();
							$aParameter['position'] = (String) $parameter['position'];
							$aParameter['name'] = (String) $parameter['name'];
							$aParameter['value'] = (String) $parameter;
							array_push($aParameters, $aParameter);
						}
						$aActions[$actionname] = $aParameters;
					}
					$aEvents[$eventname] = $aActions;
				}
					
				$aSourceEvents = $aEvents;
	// 			array_push($aSourceEvents, $aSourceEvent);
			}
			return $aSourceEvents;
		}
	
		
		
	public function getMapTargetsArray() {
		if ($this->MapTargets = array(0)) {
			$this->processMapTargets();
		}
		return $this->MapTargets;
	}
	public function getMapTargetsJSON() {
		if ($this->MapTargets = array(0)) {
			$this->processMapTargets();
		}
		return json_encode($this->MapTargets);
	}
	public function processMapTargets() {
		$objMapTargets = $this->xml->xpath(XP_MAP_MAPTARGETS);
		if ($objMapTargets) {
			
			$aMapTargets = array();
			foreach ($objMapTargets[0]->MapTarget as $MapTarget) {
				$maptargetname = (String) $MapTarget['name'];
				
// 				$ThisMapTarget = array();
// 				$ThisMapTarget['name'] = $maptargetname;
				
				$MapSchema = $MapTarget->MapSchema;
				if ($MapSchema->MapExpressions) {
					
					$aMapExpressions = array();
					foreach($MapSchema->MapExpressions->MapExpression as $MapExpression) {
						$MapExpPriKey = strtolower((String) $MapExpression['recordlayoutname']) . '-' . strtolower((String) $MapExpression['fieldname']);
						$hashKey = hash("md5", $MapExpPriKey);
						//$this->MapExpressions[$hashKey] = $MapExpression;
						$aMapExpressions[$hashKey] = $MapExpression;
					}
				}
				
				$Schema = $MapSchema->Schema;
				$aRecordLayouts = array();
				if ($Schema->RecordLayouts) {
					
					foreach($Schema->RecordLayouts->RecordLayout as $RecordLayout) {
						$recordlayoutname = (String) $RecordLayout['name'];
						
						$ThisRecordLayout = array();
						$ThisRecordLayout['name'] = $recordlayoutname;
						
						$TargetFields = array();
						if ($RecordLayout->Fields) {
							
							$Fields = $this->extractRecordLayouts($RecordLayout->Fields);
							
							//Now associate expressions with it's corresponding field (only for MapTargets)
							foreach($Fields as $Field) {
								$fieldname = (String) $Field['name'];
								$MapExpPriKey = strtolower($recordlayoutname) . '-' . strtolower($fieldname);
								$hashKey = hash("md5", $MapExpPriKey);
								if (array_key_exists($hashKey, $aMapExpressions)) {
									//$fieldexpression = '<pre><code class="prettyprint lang-vb expression-code">' . $aMapExpressions[$hashKey] . '</code></pre>' ;
									$fieldexpression = $aMapExpressions[$hashKey];
								} else {
									//$fieldexpression = '<span class="missing-tag">No Expression defined</span>';
									$fieldexpression = 'NA';
								}
								$Field['expression'] = $fieldexpression;
							}
							$TargetFields = $Fields;
						}
						$ThisRecordLayout['fields'] = $TargetFields;
						array_push($aRecordLayouts, $ThisRecordLayout);
					}
				}
				$ThisMapTarget['layouts'] = $aRecordLayouts;
// 				array_push($aMapTargets, $ThisMapTarget);
				$aMapTargets[$maptargetname] = $ThisMapTarget;
			}
			$this->MapTargets = $aMapTargets;
		}
	}
	
	function extractRecordLayouts($Fields) {
		$TargetFields = array();
		foreach($Fields->Field as $Field) {
			$fieldname = (String) $Field['name'];
		
			$TargetField = array();
			$TargetField['name'] = $fieldname;
			$TargetField['datatype'] = (String) $Field->Datatype['datatype'];
			$TargetField['dataalias'] = (String) $Field->Datatype['dataalias'];
			$TargetField['dataalign'] = (String) $Field->Datatype['dataalign'];
			$TargetField['datalength'] = (String) $Field->Datatype['datalength'];
			$TargetField['dataidentify'] = (String) $Field->Datatype['dataidentify'];
			$TargetField['dataautoinc'] = (String) $Field->Datatype['dataautoinc'];
			$TargetField['datacurrency'] = (String) $Field->Datatype['datacurrency'];
			$TargetField['datarowversion'] = (String) $Field->Datatype['datarowversion'];
			$TargetField['datapadchar'] = (String) $Field->Datatype['datapadchar'];
		
			array_push($TargetFields, $TargetField);
		}
		
		return $TargetFields;
	}
	

}
