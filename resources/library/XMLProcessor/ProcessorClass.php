<?php
include_once 'RenderMapClass.php';

class ProcessorClass extends RenderMapClass {

// 	public $obj;

	function __construct($path_to_xml_file) {
		parent::__construct($path_to_xml_file);
// 		try {
// 			$this->obj = new RenderMap($xmlSource);
// 		} catch (Exception $e) {
// 			echo $e->getMessage();
// 		}
		
// 		if (!$this->obj) {
// 			echo 'Unable to create Render class object. Action aborted.';
// 		} 
	}

	public function displayRecordLayoutEventsTable($recordLayoutName='Delimited') {
		$aRecordLayoutEvents = Parent::getRecordLayoutEventsArray($recordLayoutName);
		echo var_export($aRecordLayoutEvents, true);
		echo '<h1>Layout Name: ' . $recordLayoutName . '</h1>' ;
		foreach ($aRecordLayoutEvents as $eventname => $Event){
			echo '<div class="sourcevents-table"><table><thead><tr><td colspan="3">Action [ ' . $eventname . ' ]</td><tr>'.
					'<tr><td>Action</td><td>Item</td><td>Description</td></tr></thead><tbody>';
			foreach ($Event as $actioname => $SourceAction){
// 			echo var_export($SourceAction, $actioname);
				echo $this->decodeParameter($SourceAction, $actioname);
			}
			echo '</tbody></table></div>';
		}
	
	}
	
	public function displaySourceEventsTable() {
		$aSourceEvents = Parent::getMapEventsArray();
		foreach ($aSourceEvents as $Event){
			echo '<div class="sourcevents-table"><table><thead><tr><td colspan="3">Action [ ' . $Event['name'] . ' ]</td><tr>'.
			'<tr><td>Action</td><td>Item</td><td>Description</td></tr></thead><tbody>';
			foreach ($Event['actions'] as $SourceAction){
				echo $this->decodeParameter($SourceAction);
			}
			echo '</tbody></table></div>';
		}
		
	}
		
		public function decodeParameter($Action, $ActionName=null) {
			$additionalParamsTable = null;
			
			if (!$ActionName) {
				$ActionName = $Action['name'];
				$aParamsArray = $Action['params'];
			} else {
				$aParamsArray = $Action;
			}
			
			$output = '<tr><td>' . $ActionName . '</td>';

			$lactionname = strtolower($ActionName);
			if ($lactionname == "execute" | $lactionname == "count") { 
					foreach ($aParamsArray as $key => $SourceParameter){
						$output .= '<td>' . $SourceParameter['name'] . '</td>' . 
						'<td><pre><code class="prettyprint lang-vb">' . $SourceParameter['value'] . '</code></pre></td>';
					}
			} else if( $lactionname == "clearmapput record") {
					$output .= '<td>';
					foreach ($aParamsArray as $key => $SourceParameter){
						if ($SourceParameter['name'] == 'record layout') {
								$output .= '<h3>' . $SourceParameter['value'] . '</h3>';
						} else if ($SourceParameter['name'] == 'target name') {
							$output .= $SourceParameter['value'];
						} else {
							if (!$additionalParamsTable) {
								$additionalParamsTable = '<br /><span>Additional Params</span>';
							}
							$additionalParamsTable .= $SourceParameter['name'] . 
							                                  '->' . $SourceParameter['value']; 
						}
					}
					$output .= '</td>';
			} else {
				$output .= '<td>-- No Parameters declared --</td><td>-- No Description declared --</td>';
			}

			if ($additionalParamsTable) {
				$output .= $additionalParamsTable;
			}
			
			return $output . '</tr>';
		}
}

// echo $obj->xmlpath . '<br />';
// echo 'Description Tag Exists?:' . (( $obj->isDescriptionTagExist() > 0 ) ? 'Yes' : 'No' );
// $varCnt = $obj->isVariablesTagExist();
// echo '<br />Variables Tag Exists?:' . (( $varCnt > 0 ) ? ('Yes Count: ' . $varCnt) : 'No' );
// $varCnt = $obj->isMapEventsTagExist();
// echo '<br />MapEvents Tag Exists?:' . (( $varCnt > 0 ) ? ('Yes Count: ' . $varCnt) : 'No' );
// $varCnt = $obj->isMapSourcesTagExist();
// echo '<br />MapSources Tag Exists?:' . (( $varCnt > 0 ) ? ('Yes Count: ' . $varCnt) : 'No' );
// $varCnt = $obj->isMapTargetsTagExist();
// echo '<br />MapTargets Tag Exists?:' . (( $varCnt > 0 ) ? ('Yes Count: ' . $varCnt) : 'No' );
// echo '<br />MapEventJSON: <pre id="mapeventjson" class="jsonWindow"></pre>';
// echo '<br />MapSourceJSON: <pre id="mapsourcejson" class="jsonWindow"></pre>';
// echo '<br />MapTargetJSON: <pre id="maptargetjson" class="jsonWindow"></pre>';
//echo 'MapEvents Children Count:' . $obj->isDescriptionTagExist();
