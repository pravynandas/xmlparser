<?php
include_once 'ProcessorClass.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
	<title>Using PHP SimpleXml to create HTML from XML data</title>
	<meta http-equiv="Content-Type" lang="en" content="text/html; charset=utf-8" />
	<meta name="language" content="en"/>
	<meta name="author" content="Praveen Kumar Nandagiri" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="styles/style.css" title="default" media="screen" />
	<script src="/google-code-prettify/run_prettify.js"></script>
</head>
<body onload="setupPage()">
	<div id="container">
		<div id="masterbox">
			<div id="content">
				<!--Your content in here-->
				<?php
				if(!empty($_GET['xml'])){
					$xmlSource = './maps/' . $_GET['xml'];
				} else {
					$xmlSource = "./maps/MFS-V.map.xml";
				}
				
				$pagetitle = $xmlSource;
				echo '<table id="header-fixed"></table>';
				echo '<table id="table-1">';
				echo '<thead><tr><td><h1>' . str_replace('./maps/', '', str_replace('all/', '', $pagetitle)) . '</h1></td></tr></thead><tbody>';
				echo '</tbody></table>';
				echo '<table id="dummy-teable" style="display:none"><thead><tr><td></td></tr></thead></table>';
				
				
				try {
					$obj = new Processor($xmlSource);
				} catch (Exception $e) {
					echo $e->getMessage();
				}
				
				echo $obj->displayRecordLayoutEventsTable();
// 				echo $obj->displaySourceEventsTable();
				
				echo '<br />MapEventJSON: <pre id="mapeventjson" class="jsonWindow"></pre>';
				echo '<br />MapSourceJSON: <pre id="mapsourcejson" class="jsonWindow"></pre>';
				echo '<br />MapTargetJSON: <pre id="maptargetjson" class="jsonWindow"></pre>';
				
				?>
				
				
			</div><!--end content-->
		</div><!--end masterbox-->
	</div><!--end container-->
	
	<script>
	    var iScrollPosX, iScrollPosY;
		iScrollPosX = 0;
		iScrollPosY = 0;
		
		function getLastPos() {
			//alert('hello world');
			var doc = document.documentElement;
			var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
			var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
			iScrollPosX = left;
			iScrollPosY = top;
			//alert(iScrollPosX + ',' + iScrollPosY);
			return true;
		}
		
		function gotoLastPost() {
			//alert(iScrollPosX + ',' + iScrollPosY);
			window.scrollTo(iScrollPosX, iScrollPosY);
		}
		
		function enablescroll(id) {
			if (document.getElementById(id).style.overflow != "hidden") {
				document.getElementById(id).scrollTo(0,0);
				document.getElementById(id).style.overflow = "hidden";
			} else {
				document.getElementById(id).style.overflow = "scroll";
			}
		}
		
		function setupPage() {
			//reset all div tables scroll to top
		}
		

		/*To highlight Json syntax*/
		function output(id, inp) {
			document.getElementById(id).innerHTML = inp;
		}
		function syntaxHighlight(json) {
		    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		        var cls = 'number';
		        if (/^"/.test(match)) {
		            if (/:$/.test(match)) {
		                cls = 'key';
		            } else {
		                cls = 'string';
		            }
		        } else if (/true|false/.test(match)) {
		            cls = 'boolean';
		        } else if (/null/.test(match)) {
		            cls = 'null';
		        }
		        return '<span class="' + cls + '">' + match + '</span>';
		    });
		}
		
		var objEvents = <?php  echo $obj->getMapEventsJSON(); ?>;
		var str = JSON.stringify(objEvents, undefined, 4);
		output('mapeventjson', syntaxHighlight(str));

		var objSources = <?php  echo $obj->getMapSourcesJSON(); ?>;
		str = JSON.stringify(objSources, undefined, 4);
		output('mapsourcejson', syntaxHighlight(str));

		objTargets = <?php  echo $obj->getMapTargetsJSON(); ?>;
		str = JSON.stringify(objTargets, undefined, 4);
		output('maptargetjson', syntaxHighlight(str));



		/* to display map name on top of the page */
		$(function(){
		
		var tableOffset = $("#table-1").offset().top;
		var $header = $("#table-1 > thead").clone();
		var $fixedHeader = $("#header-fixed").append($header);
		 
			$(window).bind("scroll", function() {
			    var offset = $(this).scrollTop();
			    //alert(offset);
			    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
			        $fixedHeader.show();
			    }
			    else if (offset < tableOffset) {
			        $fixedHeader.hide();
			    }
			});

		});
	</script>
</body>
</html>