<?php
namespace db2dao;

class ConnectionFactory {

	protected static $connection;
	
	//private $schema = 'xx_q7csdb';
	private $schema = 'pervasiv';
	private $username = "db2admin";
	private $password = "db2@dmin";

	public function getConnection() {
		if (!self::$connection) {
			self::$connection = db2_pconnect ($this->schema, $this->username, $this->password);
		}
		return self::$connection;
	}

}

class csdbConnect {

	protected $connectionFactory;

	public function __construct(ConnectionFactory $factory = null) {
		if (!$factory) {
			$factory  = new ConnectionFactory;
		}
		$this->connectionFactory = $factory;
	}

	public function getConnection() {
		$connection = $this->connectionFactory->getConnection();
		
		if ($connection) {
			return $connection;
		}
	}

}

class dbconnect{
	
	public $connection;
	
	function __construct()
	{
		//$this->connection = mysqli_connect(tsDatabaseHost, tsDatabaseUser, tsDatabasePassword, tsDatabaseDb, tsDatabasePort);
		//$dbconn = mysqli_connect(tsDatabaseHost, tsDatabaseUser, tsDatabasePassword, tsDatabaseDb, tsDatabasePort);
		$database = "DRIVER={IBM DB2 ODBC DRIVER};" . 
					"DATABASE=" . tsDb2DatabaseDb . 
					";HOSTNAME=" . tsDb2DatabaseHost .
					";PORT=" . tsDb2DatabasePort . 
					";PROTOCOL=TCPIP" . 
					";UID=" . tsDb2DatabaseUser . 
					";PWD=" . tsDb2DatabasePassword . 
					";";
		//$database = "Q1CSDB";
		$username = '';
		$password = '';	
		try{
		
			$dbconn = db2_connect($database, $username, $password);
		}catch (exception $e){
			echo 'exception: ' . $e;
		}	
		
		//var_dump($dbconn);
		if(!$dbconn){
			echo 'unable to connect in class';
		}else{
			$this->connection = $dbconn;
			echo 'successfully connected to database';
		}
			

	}
	
	function __destruct()
	{
		//mysqli_close($this->connection);
		db2_close($this->connection);
	}
	
	function Reconnect()
	{
		die();
		try{
			
			mysqli_close($this->connection);
			//echo 'connection closed successfully <br>';
			
		}catch (mysqli_sql_exception $e) {
			
     	 	throw $e;
   		} 
		
   		try{
   			
   			//echo 'now reconnnecting <br>';
   			$this->connection = mysqli_connect(tsDatabaseHost, tsDatabaseUser, tsDatabasePassword, tsDatabaseDb, tsDatabasePort);
   			
   		}catch (mysqli_sql_exception $e) {
			
     	 	throw $e;
   		}
	}
}

class sqlHandler extends dbconnect{
	
	public $err;
	public $vessels = array();
	public $profiles = array();
	public $weathers = array();
	
	public $from;
	
	public function __construct(){

		parent::__construct();
		
		$this->err = 'START';
				
		try {
			$result = mysqli_query($this->connection, 'select FullName from vesseltype where status = 1 order by SrtOrd asc;');
			if($result){
				while($row = mysqli_fetch_array($result)) {
					$this->vessels[] = $row[0] ;
				}
			}
			mysqli_free_result($result);
				
		} catch (Exception $e) {
			$this->err = $this->err . '<br/>' . $e;
		}
		
		try {
			$result = mysqli_query($this->connection, 'select UserId from profiles where status = 1 order by UserId;');
			
			if($result){
				
				while($row = mysqli_fetch_array($result)) {
					
					$this->profiles[] = $row[0] ;
					
				}
			}
			
			mysqli_free_result($result);
				
		} catch (Exception $e) {
			
			$this->err = $this->err . '<br/>' . $e;
			
		}
		
		try {
			$result = mysqli_query($this->connection, 'select FullName from weather where Status = 1 order by SrtOrd asc;');
			
			if($result){

				while($row = mysqli_fetch_array($result)) {
					$this->weathers[] = $row[0] ;
				}
			}
				
			mysqli_free_result($result);

		} catch (Exception $e) {
			$this->err = $this->err . '<br/>' . $e;
		}
		
	}
	
	
	public function getRowCount() {
			$fromwhere = ' where dDate >= \'' . $this->from . '\';';
		
// 			echo $fromwhere;
			try {
				
				$result = mysqli_query($this->connection, 'select count(*) as cnt from main ' . $fromwhere);
				if($result){  
						return  mysqli_fetch_array($result)[0];
				} else {
						return 0;
				}
				mysqli_free_result($result);
				
			} catch (Exception $e) {
				$this->err = $this->err . "<br/>" . $e;
			}
	}
		
}




// class ConnectionFactory
// {
// 	private static $factory;
// 	private $csdb;
// 	private $apollodb;

// 	public static function getFactory()
// 	{
// 		if (!self::$factory)
	// 			self::$factory = new ConnectionFactory();
	// 		return self::$factory;
	// 	}

	// 	public function getCsdbConnection($schema) {

	// 		//if (!$this->csdb)
		// 		//{
		// 			$username = 'ezcs_ro';
		// 			$password = 'ezcs_ro11';

		// 			//$this->csdb = odbc_pconnect ($schema,$username,$password);
		// 			$this->csdb = db2_connect ($schema, $username, $password);
		// 			//echo 'db is now connected again';
		// 		//}

		// 		return $this->csdb;
		// 	}

		// 	public function getApolloConnection($schema,$username,$password) {

		// 		//if(!$this->apollodb)
			// 		//{
			
			// 			//$this->apollodb = odbc_pconnect ($schema,$username,$password);
			// 			$this->apollodb = db2_connect ($schema,$username,$password);
			// 			//echo 'db is now connected again';
			// 		//}


			// 		return $this->apollodb;
			// 	}
			// }
