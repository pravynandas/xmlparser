<?php

$config = array(
		"db" => array(
				"db1" => array(
						"dbname" => "database1",
						"username" => "dbUser",
						"password" => "pa$$",
						"host" => "localhost"
				),
				"db2" => array(
						"dbname" => "database2",
						"username" => "dbUser",
						"password" => "pa$$",
						"host" => "localhost"
				)
		),
		"urls" => array(
				"baseUrl" => "http://localhost"
		),
		"paths" => array(
				"resources" => "./resources",
				"images" => array(
						"content" => $_SERVER["DOCUMENT_ROOT"] . "/img/content",
						"layout" => $_SERVER["DOCUMENT_ROOT"] . "/img/layout"
				)
		)
);
