<?php
if(!empty($_GET['xml'])){
	$archive_file_name = basename($_GET['xml']);
	$archive_file_path = realpath('outputs') . '\\' . $archive_file_name;
} else {
	echo 'Input file missing. Download aborted.';
	die();
}


if (file_exists($archive_file_path)) {
	//proceed
} else {
	echo 'Error while retrieing the informatica file [' . $archive_file_path . ']. Action aborted.';
	die();
}


	try {

		header('Content-type: text/xml');
		header('Content-Disposition: attachment; filename="text.xml"');
		readfile("$archive_file_path");

	} catch (Exception $e) {
		echo $e->getMessage();
	}
