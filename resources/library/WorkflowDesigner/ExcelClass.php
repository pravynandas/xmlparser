<?php
class ExcelClass {
	
	public $xl;
	public $wb;
	public $saveFileName;
	public $saveStatus;
	public $error;
	
	function __construct() {
		//Get TEMPLATE file path
		$templatefilename = 'template\\' . 'Layout_Fields_1.0.xls';
		
		//DECLARE EXCEL CLASS & Open TEMPLATE workbook
		
		//##DOTNET##
		//		$full_assembly_string = 'Microsoft.Office.Interop.Excel, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c';
		//		$full_class_name = 'Microsoft.Office.Interop.Excel.ApplicationClass';
		//		$xl = new DOTNET($full_assembly_string, $full_class_name);
		//		$this->wb = $xl->workbooks->open($TemplateFile);
		
		
		//##PHPExcel##
		try {
			$this->xl = PHPExcel_IOFactory::createReader('Excel5');
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		$this->wb = $this->xl->load($templatefilename);
		
		//TODO: Error handling if workbook does not exists
		if ($this->wb) {
		
		//##DOTNET##
		//Get control of template sheet
		//			$this->wstemp = $this->wb->Worksheets("Template");
		
		//Copy template sheet to a new sheet; and get control of it
		//			$this->wstemp->Copy(null, $this->wb->worksheets($this->wb->worksheets->count()));
		//			$this->ws = $this->wb->worksheets($this->wb->worksheets->count());
		
		//##PHPExcel##
		$this->ws = clone $this->wb->getActiveSheet();
		
		$this->ws->setTitle('sources');
		$this->wb->AddSheet($this->ws);
		$wsSource = $this->wb->setActiveSheetIndexByName('sources');
		
		if ($wsSource) {
			$wsSource->setCellValue("A1", 'This is Source Sheet');
		}
		} else {
			echo 'error opening template workbook';
			return;
		}
		$this->ws = clone $this->wb->getActiveSheet();
		
		$this->ws->setTitle('targets');
		$this->wb->AddSheet($this->ws);
		$wsSource = $this->wb->setActiveSheetIndexByName('targets');
		
		if ($wsSource) {
			$wsSource->setCellValue("A1", 'This is Target Sheet');
		} else {
			echo 'error opening template workbook';
			return;
		}
		
// 			$this->saveExcelFile($this->saveFileName);
	}
	
	function __destruct() {
		if ($this->saveFileName) {
			try{
				$this->saveExcelFile($this->saveFileName);
				$this->saveStatus = true;
			} catch (Exception $e) {
				$this->saveStatus = false;
				$this->error = $e->getMessage();
			}
		} else {
			$this->saveStatus = true;
			$this->saveExcelFile("test");
		}
	}
	
	function setSaveFileName($name){
		$this->saveFileName = $name;
	}
	
	function saveExcelFile($file){
		//Get current time for timestamp and prepare TARGET file name
		$micro_date = microtime();
		$date_array = explode(" ", $micro_date);
		$date = date("YmdHis", $date_array[1]);
		//TODO: incorporate file properiteis in into folder structure (Team/self/cus etc.,)
		$fileName = $file . $date . '.xls';
		//TODO: create folder structure to represent the report's properties
		//user_id
		//		-> Team
		//		-> Self
		//		-> Cust
		// 				$filePath = WWW_ROOT . 'downloads\\reports\\';
		$filePath = 'outputs\\';
		$file = $filePath . $fileName;
		// 			$fileInPdf = $filePath . $fileName . '.pdf';
		// 			$file = WWW_ROOT . 'downloads\\reports\\' . 'BoatLogPro_Report_' . $date . '.xls';

		
		//Save current workbook and close (&kill) the application
		//##DOTNET##
		//			$this->wb->SaveAs($file);
		//			$this->wb->Close();
		//			$xl->Quit();
		//			$xl = null;
		
		//##PHPExcel##
		$xlWriter = PHPExcel_IOFactory::createWriter($this->wb, 'Excel5');
		$xlWriter->save($file);
		$xlWriter = null;
	}
	
	
// 	function writeSource($layoutname, $layoutfields) {
	
// 		$sourceLayoutName = 'Input_' . $this->title . '_' . $layoutname;
// 		$this->xml_l4aS = $this->getNodeSource( $sourceLayoutName  );
// 		$this->xml_l3f->appendChild( $this->xml_l4aS );
	
// 		$this->xml_l4aS->appendChild( $this->getNodeFlatFile(true) );
	
// 		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Base Table Name', '') );
// 		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Search Specification', '') );
// 		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Sort Specification', '') );
// 		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Datetime Format', 'A  19 mm/dd/yyyy hh24:mi:ss') );
// 		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Thousand Separator', 'None') );
// 		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Decimal Separator', '.') );
// 		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Add Currently Processed Flat File Name Port', 'YES') );
	
// 		$fieldnumer = 1;
// 		foreach ($layoutfields as $layoutfield) {
// 			$this->xml_l4aS->appendChild( $this->getNodeFields($layoutfield['name'], $layoutfield['datalength'], $fieldnumer, true) );
// 			$fieldnumer = $fieldnumer + 1;
// 		}
// 		//Write file name as last parameter
// 		if ($this->bsourceFileNameCreatedInFirstMap == false) {
// 			$this->xml_l4aS->appendChild( $this->getNodeFieldCurrentProcFileName($fieldnumer) );
// 			$this->bsourceFileNameCreatedInFirstMap = true;
// 		}
// 		// 		for($i=0; $i <= 8; $i++) {
// 		// 			$this->xml_l4aS->appendChild( $this->getNodeFields(true) );
// 		// 		}
	
// 	}
	
	
// 	function writeTarget($layoutname, $layoutfields) {
// 		$this->xml_l4bT = $this->getNodeTarget( 'Output_' . $this->title . '_' . $layoutname );
// 		$this->xml_l3f->appendChild( $this->xml_l4bT );
	
// 		$this->xml_l4bT->appendChild( $this->getNodeFlatFile(false) );
// 		$fieldnumer = 1;
// 		foreach ($layoutfields as $layoutfield) {
// 			$this->xml_l4bT->appendChild( $this->getNodeFields($layoutfield['name'], $layoutfield['datalength'], $fieldnumer, false) );
// 			$fieldnumer = $fieldnumer + 1;
// 		}
// 		if ($this->btargetFileNameCreatedInFirstMap == false) {
// 			$this->xml_l4bT->appendChild( $this->getNodeFields('CurrentlyProcessedFileName', '256', $fieldnumer, false) );
// 			$this->btargetFileNameCreatedInFirstMap = true;
// 		}
// 		//Write file name as last parameter
	
// 		// 		for($i=0; $i <= 11; $i++) {
// 		// 			$this->xml_l4bT->appendChild( $this->getNodeFields('sample', '10', false) );
// 		// 		}
// 		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Datetime Format', 'A  19 mm/dd/yyyy hh24:mi:ss') );
// 		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Thousand Separator', 'None') );
// 		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Decimal Separator', '.') );
// 		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Line Endings', 'System default') );
	
// 		$this->xml_l4bT->appendChild( $this->getNodeMetaDataExtension('STRING', 'Extension', '[YYYY]', '256') );
// 		$this->xml_l4bT->appendChild( $this->getNodeMetaDataExtension('STRING', 'Extension_1', 'YYYYMMDDHHMISS', '256') );
// 		$this->xml_l4bT->appendChild( $this->getNodeMetaDataExtension('STRING', 'Extension_2', 'YYYYMMDDHHMISS', '14') );
	
// 	}
	
	
	
	
// 	function downloadXlNative($param1 = null, $param2 = null){
	
// 		$thisError = null;
// 		$index = 10;
// 		$currMonth = 1;
// 		$prevMonth = 1;
// 		$bNewSheetCreated = false;
// 		$sheetCount = 1;
// 		$sheetCountThisMonth = 1;
	
// 		//Get TEMPLATE file path
// 		$TemplateFile = WWW_ROOT . 'downloads\\template\\' . 'Master_Template_1.0.xls';
	
// 		//==========================================================================
// 		$Records = $this->LogHelp->getRecentRecords($param1, $param2);
// 		//==========================================================================
// 		//	$this->Log("Downloaded records are: " . var_export($Records, TRUE));
	
// 		if (!$Records) {
// 			$this->Session->setFlash('Sorry, no records found for the selected period [' . Inflector::humanize($param1) . '].', '', array(
// 					'class' => 'warning'
// 			));
// 			$this->redirect('/Logs/index');
// 		}
	
// 		$UserCompany = $this->Company->findById($this->UserAuth->getCompanyId());
// 		//TODO: Error handling if company information does not exist
	
// 		try {

	
// 			//Increment sheet counter
// 			$sheetCount = $sheetCount + 1;
// 			//}
	
// 			foreach ( $Records as $Record ) {
// 				//Check current record and previous record belong to same month
// 				$thisMonth = (int) date("m",strtotime($Record['Record']['dDate']));
// 				$thisMonthInMMM = strtoupper(date("M",strtotime($Record['Record']['dDate'])));
// 				$thisMonthYearInYY = date("y",strtotime($Record['Record']['dDate']));
// 				$thisDateInMMDDYY = strtoupper(date("m-d-y",strtotime($Record['Record']['dDate'])));
	
// 				if ($index > 39) {
// 					//##DOTNET##
// 					//Copy template sheet to a new sheet; RENAME it and get control of it
// 					//				$this->wstemp->Copy(null, $this->wb->worksheets($this->wb->worksheets->count()));
// 					//				$this->ws = $this->wb->worksheets($this->wb->worksheets->count());
	
// 					//##PHPExcel##
// 					//First add cloned sheet to workbook then kill it
// 					//Now, take control of template sheet again to clone it
// 					$this->wb->addSheet($this->ws);
// 					$this->ws = null;
// 					$this->wb->setActiveSheetIndexByName('Template');
// 					$this->ws = clone $this->wb->getActiveSheet();
	
// 					//Rename it
// 					//				$this->ws->Name = $thisMonthInMMM . $sheetCountThisMonth; 			//DOTNET
// 					$this->ws->setTitle($thisMonthInMMM . $sheetCountThisMonth);			//PHPExcel
	
// 					$sheetCountThisMonth = $sheetCountThisMonth + 1;
// 					$sheetCount = $sheetCount + 1;
// 					$index = 10;
// 					$bNewSheetCreated = true;
// 				}
	
// 				$currMonth = $thisMonth;
	
// 				if ($index == 10 && $sheetCount == 2) {
// 					//NOTE: This loop exeuctes only 1 time
	
// 					//read prevmonth which will be used in all future iterations
// 					$prevMonth = $thisMonth;
	
// 					//Assign FIRST sheeet name
// 					//				$this->ws->Name = $thisMonthInMMM . $sheetCountThisMonth; 			//DOTNET
// 					$this->ws->setTitle($thisMonthInMMM . $sheetCountThisMonth);			//PHPExcel
	
// 					$sheetCountThisMonth = $sheetCountThisMonth + 1;
// 				} else {
	
// 					if ($currMonth != $prevMonth) {
	
// 						if (!$bNewSheetCreated) { //allow skips below procedure if new sheet already created above
	
// 							//##DOTNET##
// 							//Copy template sheet to a new sheet; RENAME it and get control of it
// 							//				$this->wstemp->Copy(null, $this->wb->worksheets($this->wb->worksheets->count()));
// 							//				$this->ws = $this->wb->worksheets($this->wb->worksheets->count());
	
// 							//##PHPExcel##
// 							//First add cloned sheet to workbook then kill it
// 							//Now, take control of template sheet again to clone it
// 							$this->wb->addSheet($this->ws);
// 							$this->ws = null;
// 							$this->wb->setActiveSheetIndexByName('Template');
// 							$this->ws = clone $this->wb->getActiveSheet();
	
// 							$sheetCountThisMonth = 1;
	
// 							//				$this->ws->Name = $thisMonthInMMM . $sheetCountThisMonth; 			//DOTNET
// 							$this->ws->setTitle($thisMonthInMMM . $sheetCountThisMonth);			//PHPExcel
	
// 							$sheetCountThisMonth = $sheetCountThisMonth + 1;
// 							$sheetCount = $sheetCount + 1;
// 							$index = 10;
// 						}
// 					}
// 					$prevMonth = $thisMonth;
// 					$bNewSheetCreated = false; //reset for next check
// 				}
	
// 				if ($index == 10) {
// 					//##DOTNET##
// 					//				$this->ws->Range("J1")->Value = $sheetCount - 1; // Sheet No. ____
// 					//				$this->ws->Range("L1")->Value = 'n'; // Of ____
// 					//				$this->ws->Range("E3")->Value = $UserCompany['Company']['name']; // COMPANY
// 					//				$this->ws->Range("E5")->Value = $UserCompany['Company']['bridge']; // Bridge
// 					//				$this->ws->Range("H5")->Value = $UserCompany['Company']['river']; // River
// 					//				$this->ws->Range("K5")->Value = $UserCompany['Company']['route']; // ROUTE
// 					//				$this->ws->Range("H7")->Value = $thisMonthInMMM; // MONTH OF ____
// 					//				$this->ws->Range("K7")->Value = $thisMonthYearInYY; // 20___
	
// 					//##PHPExcel##
// 					$this->ws->setCellValue("J1", $sheetCount - 1); // Sheet No. ____
// 					$this->ws->setCellValue("L1", 'n'); // Of ____
// 					$this->ws->setCellValue("E3", $UserCompany['Company']['name']); // COMPANY
// 					$this->ws->setCellValue("E5", $UserCompany['Company']['bridge']); // Bridge
// 					$this->ws->setCellValue("H5", $UserCompany['Company']['river']); // River
// 					$this->ws->setCellValue("K5", $UserCompany['Company']['route']); // ROUTE
// 					$this->ws->setCellValue("H7", $thisMonthInMMM); // MONTH OF ____
// 					$this->ws->setCellValue("K7", $thisMonthYearInYY); // 20___
// 				}
	
// 				//##DOTNET##
// 				//			$this->ws->Range("A" . $index)->Value = $thisDateInMMDDYY;
// 				//			$this->ws->Range("B" . $index)->Value = $Record['Record']['tVSign'];
// 				//			$this->ws->Range("C" . $index)->Value = $Record['Record']['tGClose'];
// 				//			$this->ws->Range("D" . $index)->Value = $Record['Record']['tFOpen'];
// 				//			$this->ws->Range("E" . $index)->Value = $Record['Record']['sVKind'];
// 				//			$this->ws->Range("F" . $index)->Value = $Record['Record']['sVessel'];
// 				//			$this->ws->Range("G" . $index)->Value = $Record['Record']['tGOpen'];
// 				//			$this->ws->Range("H" . $index)->Value = '\''; //casting to text forefully
// 				//			$this->ws->Range("H" . $index)->Value = $Record['Record']['sDelay'];
// 				//			$this->ws->Range("I" . $index)->Value = $Record['Record']['iDelay'];
// 				//			$this->ws->Range("J" . $index)->Value = $Record['Record']['sRemark'];
// 				//			$this->ws->Range("K" . $index)->Value = $Record['Record']['sWeather'];
// 				//			$this->ws->Range("L" . $index)->Value = $Record['Record']['sOperator'];
	
// 				//##PHPExcel##
// 				//Note: Unlike in DOTNET, the cell format is hard to set;
// 				//hence set the time stamp as 'h:mm AM/PM' in code and mark cell as text
// 				$this->ws->setCellValue("A" . $index, $thisDateInMMDDYY);
	
// 				$timestamp = strtotime($Record['Record']['tVSign']);
// 				$this->ws->setCellValue("B" . $index, date('h:i A', $timestamp));
// 				$timestamp = strtotime($Record['Record']['tGClose']);
// 				$this->ws->setCellValue("C" . $index, date('h:i A', $timestamp));
// 				$timestamp = strtotime($Record['Record']['tFOpen']);
// 				$this->ws->setCellValue("D" . $index, date('h:i A', $timestamp));
	
// 				$this->ws->setCellValue("E" . $index, $Record['Record']['sVKind']);
// 				$this->ws->setCellValue("F" . $index, $Record['Record']['sVessel']);
	
// 				$timestamp = strtotime($Record['Record']['tGOpen']);
// 				$this->ws->setCellValue("G" . $index, date('h:i A', $timestamp));
	
// 				$this->ws->setCellValue("H" . $index, '\''); //casting to text forefully
// 				$this->ws->setCellValue("H" . $index, $Record['Record']['sDelay']);
// 				$this->ws->setCellValue("I" . $index, $Record['Record']['iDelay']);
// 				$this->ws->setCellValue("J" . $index, $Record['Record']['sRemark']);
// 				$this->ws->setCellValue("K" . $index, $Record['Record']['sWeather']);
// 				$this->ws->setCellValue("L" . $index, $Record['Record']['sOperator']);
	
// 				$index = $index + 1;
// 			}
	
// 			//NOTE: unlike in DOTNET, sheets are added to workbook only when you call below statement
// 			//#PHPExcel##
// 			$this->wb->addSheet($this->ws);
// 			$this->ws = null;
				
	
// 			//Iterate through all worksheets and set range formats
// 			//		foreach($this->wb->worksheets as $thisws) {
// 			//			if ($thisws->Name == 'Template') {
// 			//					$thisws->Visible = false; //TODO: Change it to Very hidden
// 			//			} else {
// 			//				$thisws->Range("B10:D39")->NumberFormat = 'h:mm AM/PM';
// 			//				$thisws->Range("H10:H39")->NumberFormat = "@";
// 			//				$thisws->Range("L1")->Value = $sheetCount-1;
// 			//			}
// 			//		}
	
// // 			foreach ($this->wb->getWorksheetIterator() as $thisws) {
	
// // 				try {
// // 					//Note: Since the cell format already set in desired format; let's mark it as text
// // 					$thisws->getStyle('B10:D39')->getNumberFormat()->setFormatCode('@');
// // 					// 						->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME1);
						
// // 					$thisws->getStyle('H10:H39')->getNumberFormat()->setFormatCode('@');
	
// // 					//NOTE: Since date & time are now set as text in PHPExcel version,
// // 					//Text need to be right aligned
// // 					$thisws->getStyle('A10:D39')->getAlignment()
// // 					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
// // 					$thisws->getStyle('G10:G39')->getAlignment()
// // 					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						
// // 					//Note: Unlike in WINDOWS (DOTNET), setting format on a sheets selects the range hence reset it
// // 					$thisws->setSelectedCells('A1');
						
// // 				} catch (Exception $e) {
// // 					$thisError = $thisError . $e->getMessage();
// // 				}
	
// // 				if ($thisws->getTitle() == 'Template') {
// // 					$thisws->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
// // 				} else {
// // 					$thisws->setCellValue("L1", $sheetCount-1);
// // 				}
	
// // 			}
	
// 			//Active first sheet
// 			//##DOTNET##
// 			//		$this->wb->worksheets(1)->Activate;
	
// 			//##PHPExcel##
// 			try{
// 				$this->wb->setActiveSheetIndex(0);
// 				//$this->wb->getActiveSheet()->setSelectedCells('A1');
// 			} catch (Exception $e) {
// 				$thisError = $thisError . $e->getMessage();
// 			}
	
// 		} catch (Exception $e) {
// 			$thisError = $thisError . $e->getMessage();
// 		}
	
// // 		try {
	

				
				
// // 			//TODO: Save it to PDF if requested
// // 			/* // 			$rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
// // 				$rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
// // 				// 			$rendererLibrary = 'dompdf_0-6-0_beta3';
// // 				$rendererLibrary = 'dompdf.php';
// // 				// 			$rendererLibraryPath = dirname(__FILE__).'/'. $rendererLibrary;
// // 				$rendererLibraryPath = APP . 'Vendor' . DS . 'dompdf';
	
// // 				$this->Log($rendererLibraryPath);
// // 				try {
// // 				PHPExcel_Settings::setPdfRenderer(   $rendererName,   $rendererLibraryPath  );
// // 				} catch (Exception $e) {
// // 				}
	
// // 				// 			$objPHPExcel = new PHPExcel;
// // 				//  		$objWriter = new PHPExcel_Writer_PDF($this->wb);
	
// // 				//Instead of using xls file from memory; load it from saved location.
// // 				// 			$this->wbNew = PHPExcel_IOFactory::load($file);
// // 				// 			$this->wbNew->setActiveSheetIndex(3);
// // 				$pdfWriter = PHPExcel_IOFactory::createWriter($this->wb, 'PDF');
// // 				// 			$pdfWriter->writeAllSheets();
// // 				$pdfWriter->save($fileInPdf);
// // 				*/
// // 		} catch (Exception $e) {
// // 			$thisError = $thisError . $e->getMessage();
// // 		}
	
	
// // 		if ($thisError != null) {
// // 			$this->Log('Caught exception during excel download: ' .  $thisError . '\n');
// // 		}
	
// // 		if (file_exists($file)) {
// // 			//Tag this file name in a database table to user_id for history purposes.
// // 			//TODO: Tag file properties into history table for later use (Team/Self/Cust etc.,)
// // 			$thisRecord = array();
// // 			$thisRecord['user_id'] = $this->UserAuth->getUserId();
// // 			$thisRecord['name'] = $fileName;
// // 			$thisRecord['tag1'] = Inflector::humanize($param1);
// // 			$thisRecord['tag2'] = Inflector::humanize($param2);
// // 			// 			$thisRecord['path'] = str_replace('\\\\' ,'\\', $filePath);
// // 			$thisRecord['path'] =  str_replace('\\', '/', str_replace(WWW_ROOT, '',  $filePath));
// // 			$thisRecord['created_at'] = date('Y-m-d H:i:s');
// // 			try{
// // 				$this->UserDownload->create();
// // 				if ($this->UserDownload->save($thisRecord)) {
// // 					//File saved to database successfully.
// // 				}
	
// // 			} catch(Exception $e) {
// // 				$this->Log('Unable to save user download request to database. Details are: ' .
// // 						var_export($thisRecord, TRUE) . '\n' . $e->getMessage());
// // 			}
				
	
// // 			//Push the file for download
// // 			$this->response->file(
// // 					$file,
// // 					['download' => true, 'name' => basename($file)]
// // 			);
// // 			return $this->response;
	
// // 		} else {
	
// // 			$this->Session->setFlash('Sorry, there was an error while completing the task' . $thisError);
// // 			return $this->redirect(
// // 					array('controller' => 'Logs', 'action' => 'index')
// // 			);
	
// // 		}
// 	}
	
}