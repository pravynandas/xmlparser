<?php
class CreateWorkflowTagsClass extends clsInformatica {
	
	function __construct($args=null){
		parent::__construct($args);
	}
	
	function writeHeaders() {
		
		$this->xml_l1p = $this->xml->createElement(Node_L1_POWERMART);
		$this->xml_l1p->setAttribute('CREATION_DATE', date('m/d/Y h:i:s'));
		$this->xml_l1p->setAttribute('REPOSITORY_VERSION', '182.91');
		$this->xml->appendChild( $this->xml_l1p );
			
		$this->xml_l2r = $this->xml->createElement(Node_L2_REPOSITORY);
		$this->xml_l2r->setAttribute('NAME', 'dev_95_repo');
		$this->xml_l2r->setAttribute('VERSION', '182');
		$this->xml_l2r->setAttribute('CODEPAGE', 'Latin1');
		$this->xml_l2r->setAttribute('DATABASETYPE', 'Oracle');
		$this->xml_l1p->appendChild( $this->xml_l2r );
	
		$this->xml_l3f = $this->getNodeFolder();
		$this->xml_l2r->appendChild( $this->xml_l3f );
	}
	
	function writeSource($layoutname, $layoutfields) {
	
		$sourceLayoutName = 'Input_' . $this->title . '_' . $layoutname;
		$this->xml_l4aS = $this->getNodeSource( $sourceLayoutName  );
		$this->xml_l3f->appendChild( $this->xml_l4aS );
	
		$this->xml_l4aS->appendChild( $this->getNodeFlatFile(true) );
	
		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Base Table Name', '') );
		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Search Specification', '') );
		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Sort Specification', '') );
		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Datetime Format', 'A  19 mm/dd/yyyy hh24:mi:ss') );
		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Thousand Separator', 'None') );
		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Decimal Separator', '.') );
		$this->xml_l4aS->appendChild( $this->getNodeTableAttribute('Add Currently Processed Flat File Name Port', 'YES') );
	
		$fieldnumer = 1;
		foreach ($layoutfields as $layoutfield) {
			$this->xml_l4aS->appendChild( $this->getNodeFields($layoutfield['name'], $layoutfield['datalength'], $fieldnumer, true) );
			$fieldnumer = $fieldnumer + 1;
		}
		//Write file name as last parameter
		if ($this->bsourceFileNameCreatedInFirstMap == false) {
			$this->xml_l4aS->appendChild( $this->getNodeFieldCurrentProcFileName($fieldnumer) );
			$this->bsourceFileNameCreatedInFirstMap = true;
		}
		// 		for($i=0; $i <= 8; $i++) {
		// 			$this->xml_l4aS->appendChild( $this->getNodeFields(true) );
		// 		}
	
	}
	
	function writeTarget($layoutname, $layoutfields) {
		$this->xml_l4bT = $this->getNodeTarget( 'Output_' . $this->title . '_' . $layoutname );
		$this->xml_l3f->appendChild( $this->xml_l4bT );
	
		$this->xml_l4bT->appendChild( $this->getNodeFlatFile(false) );
		$fieldnumer = 1;
		foreach ($layoutfields as $layoutfield) {
			$this->xml_l4bT->appendChild( $this->getNodeFields($layoutfield['name'], $layoutfield['datalength'], $fieldnumer, false) );
			$fieldnumer = $fieldnumer + 1;
		}
		if ($this->btargetFileNameCreatedInFirstMap == false) {
			$this->xml_l4bT->appendChild( $this->getNodeFields('CurrentlyProcessedFileName', '256', $fieldnumer, false) );
			$this->btargetFileNameCreatedInFirstMap = true;
		}
		//Write file name as last parameter
	
		// 		for($i=0; $i <= 11; $i++) {
		// 			$this->xml_l4bT->appendChild( $this->getNodeFields('sample', '10', false) );
		// 		}
		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Datetime Format', 'A  19 mm/dd/yyyy hh24:mi:ss') );
		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Thousand Separator', 'None') );
		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Decimal Separator', '.') );
		$this->xml_l4bT->appendChild( $this->getNodeTableAttribute('Line Endings', 'System default') );
	
		$this->xml_l4bT->appendChild( $this->getNodeMetaDataExtension('STRING', 'Extension', '[YYYY]', '256') );
		$this->xml_l4bT->appendChild( $this->getNodeMetaDataExtension('STRING', 'Extension_1', 'YYYYMMDDHHMISS', '256') );
		$this->xml_l4bT->appendChild( $this->getNodeMetaDataExtension('STRING', 'Extension_2', 'YYYYMMDDHHMISS', '14') );
	
	}
	
	function writeMapping() {
		$this->xml_l4cM = $this->getNodeMapping();
		$this->xml_l3f->appendChild( $this->xml_l4cM );
		$check = 1;
		foreach ($this->sourceLayoutFields as $layoutname => $aLayoutFields){
			// 			$sourceLayoutName = 'Input_' . $this->title . '_' . $layoutname;
			// 			$sourceNode = $this->xml_l4aS->getElementsByTagName($sourceLayoutName);
			// 			$fieldsIn = $this->xml_l4aS->getElementsByTagName(Node_L4A_SOURCE_SOURCEFIELD);
			// 			$fieldsIn = $aLayoutFields;
				
	
			$tranformation = $this->getNodeTransformation( Self::Transform_Type_SourceQualifier, $layoutname );
			// 			for($i=0; $i < $fieldsIn->length; $i++) {
			// 				$tranformation->appendChild( $this->getNodeTransformField('sq_' . $fieldsIn->item($i)->getAttribute('NAME'), $fieldsIn->item($i)->getAttribute('PRECISION'), 'INPUT/OUTPUT') );
			// 			}
			foreach ($aLayoutFields as $aLayoutField){
				$tranformation->appendChild( $this->getNodeTransformField('sq_' . $aLayoutField['name'], $aLayoutField['datalength'], 'INPUT/OUTPUT') );
			}
				
			if ($check == 1){
				$tranformation->appendChild( $this->getNodeTransformField('sq_File_Name', '256', 'INPUT/OUTPUT') );
				$check = $check+ 1;
			}
				
			$tranformation->appendChild( $this->getNodeTableAttribute('Sql Query', '') );
			$tranformation->appendChild( $this->getNodeTableAttribute('User Defined Join', '') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Source Filter', '') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Number Of Sorted Ports', '0') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Tracing Level', 'Normal') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Is Partitionable', 'NO') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Pre SQL', '') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Post SQL', '') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Output is deterministic', 'NO') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Output is repeatable', 'Never') );
	
			$tranformation->appendChild( $this->getNodeMetaDataExtension('NUMERIC', 'CHG_JH_ILI_POL_CNT', '', '0') );
			$this->xml_l4cM->appendChild( $tranformation );
		}
	
		$fieldsOut = $this->xml_l4bT->getElementsByTagName(Node_L4B_TARGET_TARGETFIELD);
	
	
	
	
	
	
		$tranformation = $this->getNodeTransformation( Self::Transform_Type_Expression );
		// 		$tranformation->appendChild( $this->getNodeTransformField('ex_' . $fieldsOut->item(0)->getAttribute('NAME'), $fieldsOut->item(0)->getAttribute('PRECISION'),'OUTPUT','ERROR(&apos;transformation error&apos;)', true) );
		// 		$tranformation->appendChild( $this->getNodeTransformField('ex_' . $fieldsIn->item(0)->getAttribute('NAME'), $fieldsIn->item(0)->getAttribute('PRECISION'),'INPUT','', false) );
	
		$tranformation->appendChild( $this->getNodeTransformField('ex_File_Name', '256', 'INPUT/OUTPUT','', false, 'File_Name') );
		$tranformation->appendChild( $this->getNodeTransformField('Out_File_Name', '256', 'OUTPUT',"ERROR(&apos;transformation error&apos;)", true, 'SUBSTR(File_Name, INSTR(File_Name,&apos;/&apos;,-1,1)+1) || &apos;.csv&apos;') );
		$tranformation->appendChild( $this->getNodeTransformField('V_FILE_NAME', '256', 'LOCAL VARIABLE','', true, 'setvariable($$FILE_NAME,SUBSTR(File_Name, INSTR(File_Name,&apos;/&apos;,-1,1)+1) || &apos;.csv&apos;)') );
	
		$tranformation->appendChild( $this->getNodeTableAttribute('Tracing Level', 'Normal') );
	
		$this->xml_l4cM->appendChild( $tranformation );
	
	
	
		$check = 1;
		foreach ($this->targetLayoutFields as $layoutname => $aLayoutFields){
	
			$tranformation = $this->getNodeTransformation( Self::Transform_Type_TransactionControl, $layoutname );
			foreach ($aLayoutFields as $aLayoutField){
				$tranformation->appendChild( $this->getNodeTransformField('tc_' . $aLayoutField['name'], $aLayoutField['datalength'], 'INPUT/OUTPUT') );
			}
			// 		$tranformation = $this->getNodeTransformation( Self::Transform_Type_TransactionControl );
			// 		for($i=0; $i < $fieldsOut->length; $i++) {
			// 			$tranformation->appendChild( $this->getNodeTransformField('tc_' . $fieldsOut->item($i)->getAttribute('NAME'), $fieldsOut->item($i)->getAttribute('PRECISION'), 'INPUT/OUTPUT') );
			// 		}
			if($check == 1){
				$tranformation->appendChild( $this->getNodeTransformField('tc_File_Name', '256', 'INPUT/OUTPUT') );
				$check = $check + 1;
			}
			$tranformation->appendChild( $this->getNodeTableAttribute('Transaction Control Condition', 'TC_CONTINUE_TRANSACTION') );
			$tranformation->appendChild( $this->getNodeTableAttribute('Tracing Level', 'Normal') );
	
			$this->xml_l4cM->appendChild( $tranformation );
		}
	
	
		$check = 1;
		$aINPUTInstances = array();
		foreach ($this->sourceLayoutFields as $layoutname => $aLayoutFields){
			$sourceLayoutName = 'Input_' . $this->title . '_' . $layoutname;
	
			$instance1 = $this->getNodeInstance( self::Instance_Type_Source, $sourceLayoutName , 'Source Definition' );
			$this->xml_l4cM->appendChild( $instance1 );
			$aINPUTInstances[$layoutname] = $instance1;
			$instance2 = $this->getNodeInstance( self::Instance_Type_Transformation, 'SQ_'  . $sourceLayoutName, 'Source Qualifier' );
			$instance2->appendChild( $this->getNodeInstanceAssoc( $sourceLayoutName ) );
			$this->xml_l4cM->appendChild( $instance2 );
			$aINPUTInstances['sq_' . $layoutname] = $instance2;
	
			if ($check == 1){
				$check = $check + 1;
				$instance3 = $this->getNodeInstance( self::Instance_Type_Transformation, 'EXPTRANS', 'Expression');
				$this->xml_l4cM->appendChild( $instance3 );
	
			}
		}
		$aOUTPUTInstances = array();
		foreach ($this->targetLayoutFields as $layoutname => $aLayoutFields){
			$targetLayoutName = 'OUTPUT_' . $this->title . '_' . $layoutname;
			$instance4 = $this->getNodeInstance( self::Instance_Type_Transformation, 'TCTRANS_' . $this->title . '_' . $layoutname, 'Transaction Control' );
			$this->xml_l4cM->appendChild( $instance4 );
			$aOUTPUTInstances['tc_' . $layoutname] = $instance4;
			$instance5 = $this->getNodeInstance( self::Instance_Type_Target, $targetLayoutName , 'Target Definition' );
			$this->xml_l4cM->appendChild( $instance5 );
			$aOUTPUTInstances[$layoutname] = $instance5;
		}
	
	
	
		$check = 1;
		foreach ($this->sourceLayoutFields as $layoutname => $aLayoutFields){
			$instance1 = $aINPUTInstances[$layoutname];
			$instance2 = $aINPUTInstances['sq_' . $layoutname];
			foreach ($aLayoutFields as $aLayoutField){
				$connector = $this->getNodeConnector( $instance1, $instance2, $aLayoutField['name'], 'sq_' . $aLayoutField['name'] );
				$this->xml_l4cM->appendChild( $connector );
			}
			if ($check == 1){
				$check = $check + 1;
				$connector = $this->getNodeConnector( $instance1, $instance2, 'CurrentlyProcessedFileName', 'sq_File_Name' );
				$this->xml_l4cM->appendChild( $connector );
				$connector = $this->getNodeConnector( $instance2, $instance3, 'sq_File_Name', 'ex_File_Name' );
				$this->xml_l4cM->appendChild( $connector );
			}
		}
		$check = 1;
		foreach ($this->targetLayoutFields as $layoutname => $aLayoutFields){
			$instance4 = $aOUTPUTInstances['tc_' . $layoutname];
			$instance5 = $aOUTPUTInstances[$layoutname];
			foreach ($aLayoutFields as $aLayoutField){
				$connector = $this->getNodeConnector( $instance4, $instance5, 'tc_' . $aLayoutField['name'], $aLayoutField['name'] );
				$this->xml_l4cM->appendChild( $connector );
			}
				
			if ($check == 1){
				$check = $check + 1;
				$targetLoaderInstance = $instance5;
				$connector = $this->getNodeConnector( $instance3, $instance4, 'ex_File_Name', 'tc_File_Name' );
				$this->xml_l4cM->appendChild( $connector );
				$connector = $this->getNodeConnector( $instance4, $instance5, 'tc_File_Name', 'CurrentlyProcessedFileName' );
				$this->xml_l4cM->appendChild( $connector );
			}
		}
	
		$this->xml_l4cM->appendChild( $this->getNodeTargetLoaderOrder(1, $targetLoaderInstance) );
		/*INPUT connectors*/
		// 			for($i=0; $i < $fieldsIn->length; $i++) {
		// 				$connector = $this->getNodeConnector( $instance1, $instance2, $fieldsIn->item($i)->getAttribute('NAME'), 'sq_' . $fieldsIn->item($i)->getAttribute('NAME') );
		// 				$this->xml_l4cM->appendChild( $connector );
		// 			}
		// 			$connector = $this->getNodeConnector( $instance1, $instance2, 'CurrentlyProcessedFileName', 'sq_File_Name' );
		// 			$this->xml_l4cM->appendChild( $connector );
			
		// 			for($i=0; $i < $fieldsIn->length; $i++) {
		// 				$connector = $this->getNodeConnector( $instance2, $instance3, 'sq_' . $fieldsIn->item(0)->getAttribute('NAME'), 'ex_dummy'.$i );
		// 				$this->xml_l4cM->appendChild( $connector );
		// 			}
		// 			$connector = $this->getNodeConnector( $instance2, $instance3, 'sq_CurrentlyProcessedFileName', 'ex_File_Name' );
		// 			$this->xml_l4cM->appendChild( $connector );
	
		/*OUTPUT connectors*/
		// 			for($i=0; $i < $fieldsOut->length; $i++) {
		// 				$connector = $this->getNodeConnector( $instance3, $instance4, 'ex_dummy' . $i, 'tc_' . $fieldsOut->item(0)->getAttribute('NAME') );
		// 				$this->xml_l4cM->appendChild( $connector );
		// 			}
		// 			for($i=0; $i < $fieldsOut->length; $i++) {
		// 				$connector = $this->getNodeConnector( $instance4, $instance5, 'tc_' . $fieldsOut->item($i)->getAttribute('NAME'), $fieldsOut->item($i)->getAttribute('NAME')  );
		// 				$this->xml_l4cM->appendChild( $connector );
		// 			}
		// 			$connector = $this->getNodeConnector( $instance4, $instance5, 'tc_File_Name', 'CurrentlyProcessedFileName'  );
		// 			$this->xml_l4cM->appendChild( $connector );
	
	
	
		$this->xml_l4cM->appendChild( $this->getNodeMappingVariable('$$FILE_NAME', '256') );
		$this->xml_l4cM->appendChild( $this->getNodeMappingVariable('$$FTP_SERVER_IP', '100') );
		$this->xml_l4cM->appendChild( $this->getNodeMappingVariable('$$FTP_USER', '100') );
		$this->xml_l4cM->appendChild( $this->getNodeMappingVariable('$$FTP_PASS', '100') );
	
		$this->xml_l4cM->appendChild( $this->getNodeERPInfo() );
	
		$this->xml_l4cM->appendChild( $this->getNodeMetaDataExtension('STRING', 'Created By', 'Syntel', '256') );
		$this->xml_l4cM->appendChild( $this->getNodeMetaDataExtension('STRING', 'Extension', '', '256') );
	}
	
	
}