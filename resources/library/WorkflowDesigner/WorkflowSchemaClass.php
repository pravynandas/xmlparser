<?php
class WorkflowSchemaClass {
	Const Transform_Type_SourceQualifier = 1;
	Const Transform_Type_Expression = 2;
	Const Transform_Type_TransactionControl = 3;
	Const Instance_Type_Source = 1;
	Const Instance_Type_Transformation = 2;
	Const Instance_Type_Target = 3;
	
	
	public $xml;
	public $saveFileName;
	public $xml_l1p;
	public $xml_l2r;
	public $xml_l3f;
	public $xml_l4aS;
	public $xml_l4bT;
	public $xml_l4cM;
	public $saveStatus;
	public $error;
	public $Parent;
	public $title;
	public $targetLayouts = array();
	public $targetLayoutFields = array();
	public $sourceLayouts = array();
	public $sourceLayoutFields = array();
	public $bsourceFileNameCreatedInFirstMap = false;
	public $btargetFileNameCreatedInFirstMap = false;
	
	function __construct($args=null) {
		$implementation = new DOMImplementation();
		$dtd = $implementation->createDocumentType('POWERMART', '', 'powrmart.dtd');
// 		$this->xml = new DOMDocument("1.0", "ISO-8859-1");
		$this->xml = $implementation->createDocument('', '', $dtd);
		$this->xml->encoding = 'ISO-8859-1';
		$this->xml->formatOutput = true;
	}
	
	function setSaveFileName($name){
		$this->saveFileName = $name;
	}
	
	function __destruct() {
		if ($this->saveFileName) {
			try{
				$this->xml->save("outputs/" . $this->saveFileName);
				$this->saveStatus = true;
			} catch (Exception $e) {
				$this->saveStatus = false;
				$this->error = $e->getMessage();
			}
		} else {
			$this->saveStatus = true;
			$this->xml->save("outputs/test.xml");
		}
	}
	
	function getSaveStatus() {
		return $this->saveStatus;
	}

	function setTitle($name='New_Input_file'){
		$this->title = $name;
	}
	
	

	function prepareElement($nodename, $attr=array()) {
		$element = $this->xml->createElement( $nodename );
		foreach ($attr as $source => $val) {
			$element->setAttribute( $source, $val );
		}
		return $element;
	}
	
	function filterFieldNames($name) {
		$ignored = INFORMATICA_IGNORED_CHARS;
		for($i=0; $i<strlen($ignored); $i++){
			$replace = $ignored[$i];
			$name = str_replace($replace, '', $name);
		}
// 		$temp = str_replace('.', '', $name);
// 		$temp = str_replace('#', '', $temp);
		//Replace spaces with Underscore
		$name = str_replace(' ', '_', $name);
		//Append a character if first character is a numeric
		if (is_numeric(substr($name, 0, 1))){
			$name = 'x' . $name;
		}
		return $name;
	}
	
	function setTargetLayoutNames($aLayoutNames) {
		if (is_array($aLayoutNames)){
// 			foreach ($aLayoutNames as $name){
				$this->targetLayouts = $aLayoutNames;
// 			}
		}
	}
	function setTargetLayoutFields($aLayoutFields){
		$this->targetLayoutFields = $aLayoutFields;
	}
	function setSourceLayoutNames($aLayoutNames) {
		if (is_array($aLayoutNames)){
// 			foreach ($aLayoutNames as $name){
				$this->sourceLayouts = $aLayoutNames;
// 			}
		}
	}
	function setSourceLayoutFields($aLayoutFields) {
		$this->sourceLayoutFields = $aLayoutFields;
	}
	
	function getNodeFolder() {
	
		$attr = array();
		$attr['NAME'] = 'RPS';
		$attr['GROUP'] = '';
		$attr['OWNER'] = 'balakpr';
		$attr['SHARED'] = 'NOTSHARED';
		$attr['DESCRIPTION'] = '';
		$attr['PERMISSIONS'] = 'rwx---r--';
		$attr['UUID'] = 'fcb80c15-b72c-49c1-92dd-09ee6352acd5';
	
		return $this->prepareElement(Node_L3_FOLDER, $attr);
	}
	
	function getNodeSource($layoutname) {
		$attr = array();
		$attr['BUSINESSNAME'] = '';
		$attr['DATABASETYPE'] = 'Flat File';
		$attr['DBDNAME'] = 'FlatFile';
		$attr['DESCRIPTION'] = '';
		$attr['NAME'] = $layoutname;  //'INPUT_48555_file'
		$attr['OBJECTVERSION'] = '1';
		$attr['OWNERNAME'] = '';
		$attr['VERSIONNUMBER'] = '1';
	
		return $this->prepareElement(Node_L4A_SOURCE, $attr);
	}
	
	function getNodeTarget($layoutname) {
		$attr = array();
		$attr['BUSINESSNAME'] = '';
		$attr['CONSTRAINT'] = '';
		$attr['DATABASETYPE'] = 'Flat File';
		$attr['DESCRIPTION'] = '';
		$attr['NAME'] = $layoutname;
		$attr['OBJECTVERSION'] = '1';
		$attr['TABLEOPTIONS'] = '';
		$attr['VERSIONNUMBER'] = '1';
	
		return $this->prepareElement(Node_L4B_TARGET, $attr);
	}
	
	function getNodeFlatFile($bSource=true) {
	
		$attr = array();
		$attr['CODEPAGE'] 				= 'US-ASCII';
		$attr['CONSECDELIMITERSASONE'] = 'NO';
		$attr['DELIMITED'] 			= 'YES';
		$attr['DELIMITERS'] 			= ',';
		$attr['ESCAPE_CHARACTER'] 		= '';
		$attr['KEEPESCAPECHAR'] 		= 'NO';
		$attr['LINESEQUENTIAL'] 		= 'NO';
		$attr['MULTIDELIMITERSASAND'] 	= 'NO';
		$attr['NULLCHARTYPE'] 			= 'ASCII';
		$attr['NULL_CHARACTER'] 		= '*';
		$attr['PADBYTES'] 				= '1';
		$attr['QUOTE_CHARACTER'] 		= 'DOUBLE';
		$attr['REPEATABLE'] 			= 'NO';
		$attr['ROWDELIMITER'] 			= '10';
		if ($bSource) $attr['SHIFTSENSITIVEDATA'] 	= 'NO';
		$attr['SKIPROWS'] 				= '1';
		$attr['STRIPTRAILINGBLANKS'] 	= 'NO';
	
		return $this->prepareElement(Node_L4A_SOURCE_FLATFILE, $attr);
	}
	
	function getNodeMapping() {
	
		$attr = array();
		$attr['DESCRIPTION'] = '';
		$attr['ISVALID'] = 'YES';
		$attr['NAME'] = 'm_' . $this->title;
		$attr['OBJECTVERSION'] = '1';
		$attr['VERSIONNUMBER'] = '1';
	
		return $this->prepareElement(Node_L4C_MAPPING, $attr);
	}

	function getNodeTableAttribute($name='xyz', $value='abc') {
	
		$attr = array();
		$attr['NAME'] = $name;
		$attr['VALUE'] = $value;
	
		return $this->prepareElement(Node_L4_TABLEATTRIBUTE, $attr);
	}
	
	function getNodeFields($fieldname, $precision, $fieldnumer, $bSource=true) {
		
		$attr = array();
		$attr['BUSINESSNAME'] = '';
		$attr['DATATYPE'] = 'string';
		$attr['DESCRIPTION'] = '';
		$attr['FIELDNUMBER'] = $fieldnumer;
		if ($bSource) $attr['FIELDPROPERTY'] = '0';
		if ($bSource) $attr['FIELDTYPE'] = 'ELEMITEM';
		if ($bSource) $attr['HIDDEN'] = 'NO';
		$attr['KEYTYPE'] = 'NOT A KEY';
		if ($bSource) $attr['LENGTH'] = $precision;
		if ($bSource) $attr['LEVEL'] = '0';
		$attr['NAME'] = $this->filterFieldNames($fieldname);
		$attr['NULLABLE'] = 'NOTNULL';
		if ($bSource) $attr['OCCURS'] = '0';
		if ($bSource) $attr['OFFSET'] = '0';
		if ($bSource) $attr['PHYSICALLENGTH'] = $precision;
		if ($bSource) $attr['PHYSICALOFFSET'] = '0';
		$attr['PICTURETEXT'] = '';
		$attr['PRECISION'] = $precision;
		$attr['SCALE'] = '0';
		if ($bSource) $attr['USAGE_FLAGS'] = '';
		
		if ($bSource) {
			$elementName = Node_L4A_SOURCE_SOURCEFIELD;
		}else {
			$elementName = Node_L4B_TARGET_TARGETFIELD;
		}
		return $this->prepareElement($elementName, $attr);
	}
	
	function getNodeFieldCurrentProcFileName($fieldnumer, $fieldname='CurrentlyProcessedFileName') {
	
		$attr = array();
		$attr['BUSINESSNAME'] = '';
		$attr['DATATYPE'] = 'string';
		$attr['DESCRIPTION'] = '';
		$attr['FIELDNUMBER'] = $fieldnumer;
		$attr['FIELDPROPERTY'] = '4096';
		$attr['FIELDTYPE'] = 'ELEMITEM';
		$attr['HIDDEN'] = 'NO';
		$attr['KEYTYPE'] = 'NOT A KEY';
		$attr['LENGTH'] = '256';
		$attr['LEVEL'] = '0';
		$attr['NAME'] = $fieldname;
		$attr['NULLABLE'] = 'NULL';
		$attr['OCCURS'] = '0';
		$attr['OFFSET'] = '240';
		$attr['PHYSICALLENGTH'] = '256';
		$attr['PHYSICALOFFSET'] = '240';
		$attr['PICTURETEXT'] = '';
		$attr['PRECISION'] = '256';
		$attr['SCALE'] = '0';
		$attr['USAGE_FLAGS'] = '';
	
		return $this->prepareElement(Node_L4A_SOURCE_SOURCEFIELD, $attr);
	}
	
	function getNodeMetaDataExtension($type='STRING', $name='Extension', $value='[YYYY]', $length='256') {
		$attr = array();
		$attr['DATATYPE'] = $type;
		$attr['DESCRIPTION'] = '';
		$attr['DOMAINNAME'] = 'User Defined Metadata Domain';
		$attr['ISCLIENTEDITABLE'] = 'YES';
		$attr['ISCLIENTVISIBLE'] = 'YES';
		$attr['ISREUSABLE'] = 'YES';
		$attr['ISSHAREREAD'] = 'NO';
		$attr['ISSHAREWRITE'] = 'NO';
		$attr['MAXLENGTH'] = $length;
		$attr['NAME'] = $name;
		$attr['VALUE'] = $value;
		$attr['VENDORNAME'] = 'INFORMATICA';
		
		return $this->prepareElement(Node_L4_METADATAEXTENSION, $attr);
	}
	
	function getNodeTransformation($type, $layoutname="dummy") {
		
		switch ($type) {
			case self::Transform_Type_SourceQualifier:
				$name = 'SQ_INPUT_' . $this->title . '_' . $layoutname;
				$typename ='Source Qualifier'; 
			break;
			case self::Transform_Type_Expression:
				$name = 'EXPTRANS';
				$typename ='Expression';
			break;
			case self::Transform_Type_TransactionControl:
				$name = 'TCTRANS_' . $this->title . '_' . $layoutname;
				$typename ='Transaction Control';
			break;
			
			default:
				;
			break;
		}
		
		$attr = array();
		$attr['DESCRIPTION'] = ''; 
		$attr['NAME'] = $name; 
		$attr['OBJECTVERSION'] ='1'; 
		$attr['REUSABLE'] ='NO';
		$attr['TYPE'] = $typename; 
		$attr['VERSIONNUMBER'] ='1';
		
		return $this->prepareElement(Node_L5A_TRANSFORMATION, $attr);
	}
	
	function getNodeTransformField($name='dummy', $precision='10', $portType='INPUT/OUTPUT', $defaultVal='', $bExpression=false, $expression='TO_CHAR(&apos;Dummy&apos;)') {
		$attr = array();
		$attr['DATATYPE'] = 'string';
		$attr['DEFAULTVALUE'] = $defaultVal;
		$attr['DESCRIPTION'] = '';
		if ($bExpression) $attr['EXPRESSION'] = $expression;
		if ($bExpression) $attr['EXPRESSIONTYPE'] = 'GENERAL';
		$attr['NAME'] = $this->filterFieldNames($name);
		$attr['PICTURETEXT'] = '';
		$attr['PORTTYPE'] = $portType;
		$attr['PRECISION'] = $precision;
		$attr['SCALE'] = '0';
		
		return $this->prepareElement(Node_L5A_TRANSFORMATION_TRANSFORMFIELD, $attr);
	}
	
	function getNodeInstance($type, $name, $transformatinType){
		$attr = array();

		switch ($type) {
			case self::Instance_Type_Source:
				$attr['DBDNAME'] = 'FlatFile';
				$typename ='SOURCE';
				break;
			case self::Instance_Type_Transformation:
				$typename ='TRANSFORMATION';
				break;
			case self::Instance_Type_Target:
				$typename ='TARGET';
				break;
					
			default:
				;
				break;
		}
		
		
		$attr['DESCRIPTION'] = '';
		$attr['NAME'] = $name;
		$attr['TRANSFORMATION_NAME'] = $name;
		$attr['TRANSFORMATION_TYPE'] = $transformatinType;
		$attr['TYPE'] = $typename;
		
		return $this->prepareElement(Node_L5A_INSTANCE, $attr);
	}
	
	function getNodeInstanceAssoc($name){
		$attr = array();
		$attr['NAME'] = $name;
		return $this->prepareElement(Node_L5A_INSTANCE_ASSOC, $attr);
	}
	
	function getNodeConnector( $fromInstance, $toInstance, $fromFieldName='Employee_Number', $toFieldName = 'Employee_Number' ){
		
		$fromInstanceName = $fromInstance->getAttribute('NAME');
		$fromInstanceType = $fromInstance->getAttribute('TRANSFORMATION_TYPE');
		$toInstanceName = $toInstance->getAttribute('NAME');
		$toInstanceType = $toInstance->getAttribute('TRANSFORMATION_TYPE');
		
		$attr = array();
		$attr['FROMFIELD'] = $this->filterFieldNames($fromFieldName);
		$attr['FROMINSTANCE'] = $fromInstanceName;
		$attr['FROMINSTANCETYPE'] = $fromInstanceType;
		$attr['TOFIELD'] = $this->filterFieldNames($toFieldName);
		$attr['TOINSTANCE'] = $toInstanceName;
		$attr['TOINSTANCETYPE'] = $toInstanceType;
		
		return $this->prepareElement(Node_L5A_CONNECTOR, $attr);
	}
	
	function getNodeTargetLoaderOrder($order, $instance) {
		$attr = array();
		$attr['ORDER'] = $order;
		$attr['TARGETINSTANCE'] = $instance->getAttribute('NAME');
		return $this->prepareElement(Node_L5A_TARGETLOADORDER, $attr);
		
	}

	function getNodeMappingVariable($name='$$FTP_SERVER_IP', $precision='100') {
	
		$attr = array();
		$attr['AGGFUNCTION'] = 'MAX';
		$attr['DATATYPE'] = 'string';
		$attr['DEFAULTVALUE'] = '';
		$attr['DESCRIPTION'] = '';
		$attr['ISEXPRESSIONVARIABLE'] = 'NO';
		$attr['ISPARAM'] = 'NO';
		$attr['NAME'] = $name;
		$attr['PRECISION'] = $precision;
		$attr['SCALE'] = '0';
		$attr['USERDEFINED'] = 'YES';
	
		return $this->prepareElement(Node_L5A_MAPPINGVARIABLE, $attr);
	}
	
	function getNodeERPInfo() {
		return $this->prepareElement(Node_L5A_ERPINFO);
	}
	
	
}