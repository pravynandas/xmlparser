<?php
/*
 The important thing to realize is that the config file should be included in every
page of your project, or at least any page you want access to these settings.
This allows you to confidently use these settings throughout a project because
if something changes such as your database credentials, or a path to a specific resource,
you'll only need to update it here.

 I will usually place the following in a bootstrap file or some type of environment
 setup file (code that is run at the start of every page request), but they work
 just as well in your config file if it's in php (some alternatives to php are xml or ini files).
*/

/*
 Creating constants for heavily used paths makes things a lot easier.
 ex. require_once(LIBRARY_PATH . "Paginator.php")
*/


defined("RESOURCES_PATH") or define("RESOURCES_PATH", realpath(dirname(__FILE__)));
defined("LIBRARY_PATH") or define("LIBRARY_PATH", realpath(dirname(__FILE__) . '/library'));
defined("TEMPLATES_PATH") or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));
defined("XML_FILE_PATH") or define("XML_FILE_PATH", realpath(dirname(__FILE__) . '/../public_html/xml'));

defined('DEFAULT_XML_FILE') or define('DEFAULT_XML_FILE', '101063_C_CTS_V01.map.xml');

if (!defined('DEFAULT_XML_FILE_Schema1')) {
	define('DEFAULT_XML_FILE_Schema1', '101063_C_CTS_V01.map.xml');
}
if (!defined('DEFAULT_XML_FILE_Schema2')) {
	define('DEFAULT_XML_FILE_Schema2', 'wf_m_MFS_A.XML');
}

if(!defined('XML_Source_Path_Schema2')){
	define('XML_Source_Path_Schema2',  realpath(XML_FILE_PATH . '/schema2'));
}
if(!defined('XML_Source_Path_Schema1')){
	define('XML_Source_Path_Schema1', realpath(XML_FILE_PATH . '/schema1'));
}




if (!defined('XP_MAP')) {
	define('XP_MAP', '/Map');
}
if (!defined('XP_MAP_DESC')) {
	define('XP_MAP_DESC', XP_MAP . '/Description');
}
if (!defined('XP_MAP_VARS')) {
	define('XP_MAP_VARS', XP_MAP . '/Variables');
}
if (!defined('XP_MAP_MAPEVENTS')) {
	define('XP_MAP_MAPEVENTS', XP_MAP . '/MapEvents');
}
if (!defined('XP_MAP_MAPSOURCES')) {
	define('XP_MAP_MAPSOURCES', XP_MAP . '/MapSources');
}
if (!defined('XP_MAP_MAPSOURCE_SOURCEEVENTS')) {
	define('XP_MAP_MAPSOURCE_SOURCEEVENTS', XP_MAP_MAPSOURCES . '/MapSource/SourceEvents');
}
if (!defined('XP_MAP_MAPSOURCE_RECORDLAYOUTEVENTS')) {
	define('XP_MAP_MAPSOURCE_RECORDLAYOUTEVENTS', XP_MAP_MAPSOURCES . '/MapSource/RecordLayoutEvents');
}
if (!defined('XP_MAP_MAPTARGETS')) {
	define('XP_MAP_MAPTARGETS', XP_MAP . '/MapTargets');
}

